import {
    Environment,
    Network,
    RecordSource,
    Store,
    RecordSourceInspector,
    Observable,
} from 'relay-runtime';
import {SubscriptionClient} from 'subscriptions-transport-ws';

var source = new RecordSource();
const store = new Store(source);
if (RecordSourceInspector) {
    window.inspector = new RecordSourceInspector(source);
}

const fetchQuery = (operation, variables) => {
    return fetch('/graphql', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        credentials: 'same-origin',
        body: JSON.stringify({
            query: operation.text,
            variables,
        }),
    }).then(response => {
        return response.json();
    });
};

const subscriptionClient = new SubscriptionClient(`wss://${location.host}/graphql`, {
    reconnect: true,
});

const setupSubscription = (request, variables) => {
    const subscribeObservable = subscriptionClient.request({
        query: request.text,
        operationName: request.name,
        variables,
    });
    // Important: Convert subscriptions-transport-ws observable type to Relay's
    return Observable.from(subscribeObservable);
};

const environment = new Environment({
    network: Network.create(fetchQuery, setupSubscription),
    store,
});
window.environment = environment;

export default environment;