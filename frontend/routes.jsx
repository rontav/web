import React from 'react';

import {Resolver} from 'found-relay';
import {makeRouteConfig, hotRouteConfig, Redirect, Route, createRender} from 'found';
import {Environment, Network, RecordSource, Store} from 'relay-runtime';
import {queryMiddleware} from 'farce';

import {App, AppQuery} from './app.jsx';
import {Main, MainQuery} from './views/main';

export var routeConfig = hotRouteConfig(makeRouteConfig(
    <Route
        path="/"
        query={AppQuery}
        Component={App}
    >
        <Route
            query={MainQuery}
            Component={Main}
        />
        <Redirect
            from="*"
            to="/"
        />
    </Route>,
));

export const historyMiddlewares = [queryMiddleware];

export function createResolver(fetcher) {
    const environment = new Environment({
        network: Network.create((...args) => fetcher.fetch(...args)),
        store: new Store(new RecordSource()),
    });

    return new Resolver(environment);
}

export const render = createRender({});