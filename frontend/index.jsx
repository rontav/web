import 'react-hot-loader/patch';
import React, {PureComponent} from 'react';
import ReactDOM from 'react-dom';
import {BrowserProtocol} from 'farce';
import {createFarceRouter} from 'found';
import {Resolver} from 'found-relay';
import {AppContainer} from 'react-hot-loader';
if (process.env.REACT_HOT) {
    var {hot} = require('react-hot-loader/root');
}

import {historyMiddlewares, render, routeConfig} from './routes.jsx';
import environment from './environment.js';

const Router = createFarceRouter({
    historyProtocol: new BrowserProtocol(),
    historyMiddlewares,
    routeConfig,
    render,
});

class RootComponent extends PureComponent {
    render() {
        return <AppContainer><Router resolver={new Resolver(environment)} /></AppContainer>;
    }
}
const Root = process.env.REACT_HOT ? hot(RootComponent) : RootComponent;

ReactDOM.render(
    <Root />,
    document.getElementById('app'),
);