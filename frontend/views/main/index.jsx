import PropTypes from 'prop-types';
import React, {Component, PureComponent} from 'react';
import {graphql} from 'react-relay';
import classnames from 'classnames';

import Header from '../../components/header';
import Maps from '../../components/map';
import classes from './index.module.scss';

export class Main extends Component {
    static propTypes = {
        match: PropTypes.shape({
            location: PropTypes.object,
        }),
        viewer: PropTypes.shape({
            user: PropTypes.shape({
                photo: PropTypes.string,
            }),
            scooters: PropTypes.any,
            server: PropTypes.any,
        }),
    }
    render() {
        return (
            <>
                <div className={classes['Main']}>
                    <Header location={this.props.match.location} user={this.props.viewer?.user} />
                </div>
                <Maps
                    scooters={this.props.viewer?.scooters}
                    server={this.props.viewer?.server}
                />
            </>
        );
    }
}
export const MainQuery = graphql`
    query mainQuery {
        viewer {
            user {
                dbID
                photo
            }
            scooters {
                ...map_scooters
            }
            server {
                ...map_server
            }
        }
    }
`;