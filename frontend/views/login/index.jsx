import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button} from 'material-react';

import classes from './index.module.scss';

export default class LoginView extends Component {
    static propTypes = {
        lastLocation: PropTypes.string,
    }

    render() {
        var currentURL = this.props.lastLocation ? this.props.lastLocation : '/';

        return (
            <div className={classes['Login']}>
                <a className={classes['Login__link']} href={`/auth/facebook?returnTo=${currentURL}`}>
                    <Button ripple={false} flat>
                        <i className="fa fa-fw fa-facebook" />Facebook
                    </Button>
                </a>
                <div className="delimiter"><span>OR</span></div>
                <a className={classes['Login__link']} href={`/auth/google?returnTo=${currentURL}`}>
                    <Button ripple={false} flat>
                        <i className="ci ci-google" />Google
                    </Button>
                </a>
            </div>
        );
    }
}
