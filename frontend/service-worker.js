function receivePushNotification(event) {
    console.log('[Service Worker] Push Received.');

    const notificationText = event.data.text();
    const title = 'Scooter theft alert!';

    const options = {
        data: 'https://adse.ro',
        body: notificationText,
        vibrate: [200, 100, 200],
    };

    event.waitUntil(self.registration.showNotification(title, options));
}
self.addEventListener('push', receivePushNotification);

function openPushNotification(event) {
    console.log('[Service Worker] Notification click Received.', event.notification.data);

    event.notification.close();
    event.waitUntil(clients.matchAll({type: 'window'})
        .then((clients) => {
            if (clients.length > 0) {
                const someClient = clients[clients.length - 1];
                return someClient.navigate(event.notification.data)
                    .then(client => client.focus());
            }
            else {
                return clients.openWindow(event.notification.data);
            }
        }));
}

self.addEventListener('notificationclick', openPushNotification);