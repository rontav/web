import React, {Component, PureComponent} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import GoogleMapReact from 'google-map-react';
import {boundMethod} from 'autobind-decorator';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Button} from 'material-react';
import humanize from 'humanize';
import Easing from 'easing';

const TARGET_FPS = 60;
const MARKER_TRANSITION_DURATION = 0.5;
const ease = Easing(TARGET_FPS * MARKER_TRANSITION_DURATION, 'circular');

import {library} from '@fortawesome/fontawesome-svg-core';
import {
    faBatteryEmpty,
    faBatteryFull,
    faBatteryHalf,
    faBatteryQuarter,
    faBatterySlash,
    faBatteryThreeQuarters,
    faLocation,
} from '@fortawesome/pro-duotone-svg-icons';

library.add(
    faBatteryEmpty,
    faBatteryFull,
    faBatteryHalf,
    faBatteryQuarter,
    faBatterySlash,
    faBatteryThreeQuarters,
    faLocation,
);

import classes from './index.module.scss';
import {createFragmentContainer, requestSubscription, commitMutation, graphql} from 'react-relay';

class MyPositionMarker extends PureComponent {
    render() {
        return (
            <div className={classes['Map__locationCenter']}></div>
        );
    }
}

class ScooterMarker extends PureComponent {
    static propTypes = {
        '$hover': PropTypes.bool,
        battery: PropTypes.number,
        color: PropTypes.string,
    }
    static SIZE = 70;

    render() {
        return (
            <div className={classnames(classes['Map__marker'], {
                [classes['Map__marker--hover']]: this.props['$hover'],
                [classes[`Map__marker--${this.props.color}`]]: this.props.color,
            })}>
                <div className={classes['Map__marker__icon']} />
                <div className={classnames(classes['Map__marker__battery'], {
                    [classes['Map__marker__battery--red']]: !this.props.battery || this.props.battery < 25,
                    [classes['Map__marker__battery--orange']]: this.props.battery >= 25 && this.props.battery < 50,
                    [classes['Map__marker__battery--yellow']]: this.props.battery >= 50 && this.props.battery < 75,
                    [classes['Map__marker__battery--green']]: this.props.battery >= 75,
                })}>
                    <FontAwesomeIcon rotation={270} fixedWidth icon={['fad', classnames({
                        'battery-full': this.props.battery >= 90,
                        'battery-three-quarters': this.props.battery >= 75 && this.props.battery < 90,
                        'battery-half': this.props.battery >= 50 && this.props.battery < 75,
                        'battery-quarter': this.props.battery >= 25 && this.props.battery < 50,
                        'battery-empty': this.props.battery >= 10 && this.props.battery < 25,
                        'battery-slash': !this.props.battery || this.props.battery < 10,
                    })]} />
                </div>
            </div>
        );
    }
}

class GoogleMaps extends Component {
    static propTypes = {
        relay: PropTypes.any,
        scooters: PropTypes.shape({
            list: PropTypes.arrayOf(PropTypes.shape({
                dbID: PropTypes.any,
                battery: PropTypes.number,
            })),
        }),
        server: PropTypes.shape({
            serverTimestamp: PropTypes.number,
        }),
    };
    static subscription = graphql`
        subscription mapSubscription {
            scooterLocationUpdate {
                dbID
                scooterID
                status
                location {
                    serverTimestamp
                    latitude
                    longitude
                    hdop
                }
                info {
                    serverTimestamp
                    batteryLevel
                    adseroVoltage
                    scooterSpeed
                }
            }
        }
    `
    static distanceToMouse(markerPosition, mousePosition, markerProps) {
        if (!markerProps.size) {
            return 99999;
        }
        const x = markerPosition.x + markerProps.size.width / 2;
        const y = markerPosition.y + markerProps.size.height / 2;

        return Math.sqrt(Math.pow(x - mousePosition.x, 2) + Math.pow(y - mousePosition.y, 2));
    }
    static MIN_AUTO_ZOOM = 17;
    static STEPS = TARGET_FPS * MARKER_TRANSITION_DURATION;

    state = {
        animation: {},
    }
    @boundMethod
    handlePositionUpdate(position) {
        if (!position) {
            return;
        }

        this.myPositionCircle?.setCenter({
            lat: position?.coords?.latitude || 0,
            lng: position?.coords?.longitude || 0,
        });
        this.myPositionCircle?.setRadius(position?.coords?.accuracy);

        const firstPositionUpdate = !this.state.userPosition;

        this.setState({
            userPosition: position,
            geolocation: 'active',
        }, () => {
            if (firstPositionUpdate) {
                this.onMyPositionClick(true);
            }
        });
    }
    @boundMethod
    onMyPositionClick(firstPositionUpdate) {
        if (this.state.geolocation === 'error') {
            this.startGeolocation();
        }

        if (this.state.userPosition) {
            this.setState({
                scooterOverlay: false,
            });

            if (firstPositionUpdate === true) {
                this.zoomToFit(true);
            }
            else {
                this.map?.panTo({
                    lat: this.state.userPosition.coords.latitude,
                    lng: this.state.userPosition.coords.longitude,
                });
            }
        }
    }
    @boundMethod
    getMapBounds(scooters) {
        if (!this.maps) {
            return;
        }
        const bounds = new this.maps.LatLngBounds();

        (scooters || []).forEach(({location}) => {
            bounds.extend(new this.maps.LatLng(location.latitude, location.longitude));
        });
        return bounds;
    }
    @boundMethod
    latLng2Point(latLng) {
        var topRight = this.map.getProjection().fromLatLngToPoint(this.map.getBounds().getNorthEast());
        var bottomLeft = this.map.getProjection().fromLatLngToPoint(this.map.getBounds().getSouthWest());
        var scale = Math.pow(2, this.map.getZoom());
        var worldPoint = this.map.getProjection().fromLatLngToPoint(latLng);
        return new this.maps.Point((worldPoint.x - bottomLeft.x) * scale, (worldPoint.y - topRight.y) * scale);
    }
    @boundMethod
    point2LatLng(point) {
        var topRight = this.map.getProjection().fromLatLngToPoint(this.map.getBounds().getNorthEast());
        var bottomLeft = this.map.getProjection().fromLatLngToPoint(this.map.getBounds().getSouthWest());
        var scale = Math.pow(2, this.map.getZoom());
        var worldPoint = new this.maps.Point(point.x / scale + bottomLeft.x, point.y / scale + topRight.y);
        return this.map.getProjection().fromPointToLatLng(worldPoint);
    }
    @boundMethod
    mapLoaded({map, maps}) {
        this.map = map;
        this.maps = maps;

        this.myPositionButton = document.createElement('div');
        this.myPositionButton.className = classes['Map__myLocation'];
        this.myPositionButton.onclick = this.onMyPositionClick;
        ReactDOM.render(<FontAwesomeIcon icon={['fad', 'location']} />, this.myPositionButton);

        this.map.controls[maps.ControlPosition.RIGHT_BOTTOM] = [this.myPositionButton];

        if (!this.myPositionCircle) {
            this.myPositionCircle = new this.maps.Circle({
                strokeColor: '#4285f4',
                strokeOpacity: 0.6,
                strokeWeight: 1,
                fillColor: '#4285f4',
                fillOpacity: 0.2,
                map: this.map,
                center: {
                    lat: 0,
                    lng: 0,
                },
                radius: 0,
                cursor: 'invalid-value',
            });

            const flightPlanCoordinates = [
                {lat: 44.429663, lng: 26.09609},
                {lat: 44.429667, lng: 26.09607},
                {lat: 44.429674, lng: 26.096023},
                {lat: 44.42983, lng: 26.095777},
                {lat: 44.430027, lng: 26.095525},
                {lat: 44.430448, lng: 26.095299},
                {lat: 44.430803, lng: 26.094954},
                {lat: 44.431086, lng: 26.094526},
                {lat: 44.431367, lng: 26.094214},
                {lat: 44.431749, lng: 26.093758},
                {lat: 44.432062, lng: 26.093407},
                {lat: 44.432322, lng: 26.09309},
                {lat: 44.432574, lng: 26.092657},
                {lat: 44.432788, lng: 26.092176},
                {lat: 44.43292, lng: 26.091592},
                {lat: 44.433, lng: 26.091111},
                {lat: 44.433023, lng: 26.090617},
                {lat: 44.433017, lng: 26.090112},
                {lat: 44.433107, lng: 26.089627},
                {lat: 44.433141, lng: 26.088955},
                {lat: 44.43316, lng: 26.088414},
                {lat: 44.433203, lng: 26.087817},
                {lat: 44.433274, lng: 26.087208},
                {lat: 44.433281, lng: 26.086606},
                {lat: 44.433364, lng: 26.085989},
                {lat: 44.433394, lng: 26.08541},
                {lat: 44.433444, lng: 26.084866},
                {lat: 44.433504, lng: 26.084284},
                {lat: 44.433474, lng: 26.084041},
                {lat: 44.433473, lng: 26.084041},
                {lat: 44.433474, lng: 26.084041},
                {lat: 44.433475, lng: 26.084041},
                {lat: 44.433475, lng: 26.084041},
                {lat: 44.433477, lng: 26.084041},
                {lat: 44.433506, lng: 26.084026},
                {lat: 44.43348, lng: 26.084023},
                {lat: 44.433482, lng: 26.084017},
                {lat: 44.433435, lng: 26.084001},
                {lat: 44.43347, lng: 26.08354},
                {lat: 44.433467, lng: 26.082917},
                {lat: 44.433504, lng: 26.082275},
                {lat: 44.433537, lng: 26.081676},
                {lat: 44.433606, lng: 26.081119},
                {lat: 44.433738, lng: 26.080339},
                {lat: 44.433809, lng: 26.079844},
                {lat: 44.433903, lng: 26.07937},
                {lat: 44.434034, lng: 26.078831},
                {lat: 44.43416, lng: 26.078466},
                {lat: 44.434356, lng: 26.077833},
                {lat: 44.434582, lng: 26.07741},
                {lat: 44.434582, lng: 26.07741},
                {lat: 44.434584, lng: 26.077409},
                {lat: 44.434585, lng: 26.077408},
                {lat: 44.43458, lng: 26.077411},
                {lat: 44.434589, lng: 26.077394},
                {lat: 44.434565, lng: 26.077363},
                {lat: 44.434522, lng: 26.077405},
                {lat: 44.434641, lng: 26.077233},
                {lat: 44.43476, lng: 26.076861},
                {lat: 44.434913, lng: 26.07639},
                {lat: 44.435128, lng: 26.075787},
                {lat: 44.435316, lng: 26.075263},
                {lat: 44.43559, lng: 26.074683},
                {lat: 44.435765, lng: 26.074313},
                {lat: 44.435811, lng: 26.074219},
                {lat: 44.435852, lng: 26.074187},
                {lat: 44.436006, lng: 26.07392},
                {lat: 44.436207, lng: 26.073477},
                {lat: 44.436422, lng: 26.073039},
                {lat: 44.436692, lng: 26.072637},
                {lat: 44.436968, lng: 26.072213},
                {lat: 44.437178, lng: 26.071769},
                {lat: 44.437402, lng: 26.071203},
                {lat: 44.437582, lng: 26.070863},
                {lat: 44.437766, lng: 26.070444},
                {lat: 44.437958, lng: 26.070079},
                {lat: 44.438037, lng: 26.069815},
                {lat: 44.438136, lng: 26.069613},
                {lat: 44.438144, lng: 26.069588},
                {lat: 44.438107, lng: 26.069574},
                {lat: 44.43808, lng: 26.06956},
                {lat: 44.438191, lng: 26.069655},
                {lat: 44.438274, lng: 26.069748},
                {lat: 44.43829, lng: 26.069548},
                {lat: 44.438239, lng: 26.069497},
                {lat: 44.438254, lng: 26.06941},
                {lat: 44.438258, lng: 26.069346},
                {lat: 44.438197, lng: 26.069227},
                {lat: 44.438116, lng: 26.069169},
                {lat: 44.438027, lng: 26.069084},
                {lat: 44.437896, lng: 26.069038},
                {lat: 44.437743, lng: 26.068721},
                {lat: 44.437613, lng: 26.068406},
                {lat: 44.437537, lng: 26.068143},
                {lat: 44.437521, lng: 26.067995},
                {lat: 44.437494, lng: 26.067875},
                {lat: 44.437351, lng: 26.06754},
                {lat: 44.43724, lng: 26.067273},
                {lat: 44.437188, lng: 26.067052},
                {lat: 44.437132, lng: 26.066868},
                {lat: 44.437047, lng: 26.066776},
                {lat: 44.436877, lng: 26.066522},
                {lat: 44.436707, lng: 26.066192},
                {lat: 44.436627, lng: 26.065861},
                {lat: 44.436674, lng: 26.065617},
                {lat: 44.436448, lng: 26.065424},
                {lat: 44.436256, lng: 26.065131},
                {lat: 44.436183, lng: 26.064822},
                {lat: 44.436026, lng: 26.064521},
                {lat: 44.435921, lng: 26.064201},
                {lat: 44.435734, lng: 26.063781},
                {lat: 44.435616, lng: 26.06347},
                {lat: 44.435539, lng: 26.063118},
                {lat: 44.435412, lng: 26.0627},
                {lat: 44.435374, lng: 26.062296},
                {lat: 44.435281, lng: 26.061836},
                {lat: 44.435219, lng: 26.061563},
                {lat: 44.435207, lng: 26.061366},
                {lat: 44.435139, lng: 26.061172},
                {lat: 44.435047, lng: 26.06087},
                {lat: 44.434978, lng: 26.060652},
                {lat: 44.434944, lng: 26.060478},
                {lat: 44.434915, lng: 26.060252},
                {lat: 44.434816, lng: 26.060034},
                {lat: 44.434805, lng: 26.059744},
                {lat: 44.434811, lng: 26.05961},
                {lat: 44.434652, lng: 26.05957},
                {lat: 44.434559, lng: 26.059666},
                {lat: 44.434585, lng: 26.059715},
                {lat: 44.434534, lng: 26.059639},
                {lat: 44.434395, lng: 26.059553},
                {lat: 44.43441, lng: 26.05934},
                {lat: 44.434438, lng: 26.059139},
                {lat: 44.434438, lng: 26.059138},
                {lat: 44.434437, lng: 26.059137},
                {lat: 44.43443, lng: 26.059125},
                {lat: 44.434429, lng: 26.059121},
                {lat: 44.434442, lng: 26.059124},
                {lat: 44.434458, lng: 26.059127},
                {lat: 44.434435, lng: 26.059126},
                {lat: 44.43448, lng: 26.059014},
                {lat: 44.434481, lng: 26.05889},
                {lat: 44.434497, lng: 26.058849},
                {lat: 44.434513, lng: 26.058873},
                {lat: 44.434551, lng: 26.058871},
                {lat: 44.434591, lng: 26.058485},
                {lat: 44.434607, lng: 26.058083},
                {lat: 44.43466, lng: 26.057694},
                {lat: 44.434678, lng: 26.057259},
                {lat: 44.434553, lng: 26.056667},
                {lat: 44.434532, lng: 26.056203},
                {lat: 44.434523, lng: 26.055674},
                {lat: 44.434392, lng: 26.055249},
                {lat: 44.434352, lng: 26.054707},
                {lat: 44.434333, lng: 26.05409},
                {lat: 44.43438, lng: 26.053968},
                {lat: 44.434411, lng: 26.053974},
                {lat: 44.434414, lng: 26.053975},
                {lat: 44.434425, lng: 26.053978},
                {lat: 44.434451, lng: 26.053969},
                {lat: 44.43448, lng: 26.053963},
                {lat: 44.43442, lng: 26.05394},
                {lat: 44.434349, lng: 26.053898},
                {lat: 44.434383, lng: 26.053898},
                {lat: 44.43445, lng: 26.053971},
                {lat: 44.43452, lng: 26.053965},
                {lat: 44.434469, lng: 26.053889},
                {lat: 44.434483, lng: 26.053539},
                {lat: 44.434433, lng: 26.053111},
                {lat: 44.434407, lng: 26.052481},
                {lat: 44.434369, lng: 26.052085},
                {lat: 44.434366, lng: 26.051698},
                {lat: 44.434366, lng: 26.051237},
                {lat: 44.434252, lng: 26.050732},
                {lat: 44.434272, lng: 26.050169},
                {lat: 44.434285, lng: 26.049769},
                {lat: 44.434305, lng: 26.049423},
                {lat: 44.434249, lng: 26.04895},
                {lat: 44.434322, lng: 26.048279},
                {lat: 44.434263, lng: 26.047776},
                {lat: 44.434243, lng: 26.047309},
                {lat: 44.434239, lng: 26.04678},
                {lat: 44.434158, lng: 26.046408},
                {lat: 44.434148, lng: 26.045945},
                {lat: 44.434137, lng: 26.045943},
                {lat: 44.434133, lng: 26.045944},
                {lat: 44.434114, lng: 26.045955},
                {lat: 44.434078, lng: 26.045937},
                {lat: 44.434118, lng: 26.04593},
                {lat: 44.434145, lng: 26.045959},
                {lat: 44.434134, lng: 26.045661},
                {lat: 44.434202, lng: 26.045242},
                {lat: 44.434223, lng: 26.044797},
                {lat: 44.434181, lng: 26.044221},
                {lat: 44.434166, lng: 26.043732},
                {lat: 44.434194, lng: 26.043354},
                {lat: 44.434178, lng: 26.042936},
                {lat: 44.434148, lng: 26.042545},
                {lat: 44.434156, lng: 26.04215},
                {lat: 44.434131, lng: 26.041702},
                {lat: 44.434247, lng: 26.04134},
                {lat: 44.434326, lng: 26.040958},
                {lat: 44.434276, lng: 26.040438},
                {lat: 44.434236, lng: 26.039843},
                {lat: 44.434229, lng: 26.039329},
                {lat: 44.434168, lng: 26.038888},
                {lat: 44.434165, lng: 26.038421},
                {lat: 44.434164, lng: 26.037922},
                {lat: 44.434144, lng: 26.037202},
                {lat: 44.434163, lng: 26.036694},
                {lat: 44.434127, lng: 26.035944},
                {lat: 44.43409, lng: 26.035525},
                {lat: 44.433956, lng: 26.034977},
                {lat: 44.434006, lng: 26.034556},
                {lat: 44.433989, lng: 26.034292},
                {lat: 44.433989, lng: 26.034265},
                {lat: 44.434003, lng: 26.034266},
                {lat: 44.434008, lng: 26.034212},
                {lat: 44.433959, lng: 26.034137},
                {lat: 44.434149, lng: 26.034133},
                {lat: 44.434101, lng: 26.033924},
                {lat: 44.434166, lng: 26.033621},
                {lat: 44.434065, lng: 26.033328},
                {lat: 44.433976, lng: 26.033179},
                {lat: 44.43398, lng: 26.03307},
                {lat: 44.433971, lng: 26.032958},
                {lat: 44.433874, lng: 26.032781},
                {lat: 44.433897, lng: 26.032758},
                {lat: 44.433865, lng: 26.032771},
                {lat: 44.43384, lng: 26.032734},
                {lat: 44.433791, lng: 26.032709},
                {lat: 44.433758, lng: 26.032718},
                {lat: 44.432606, lng: 26.03252},
                {lat: 44.43222, lng: 26.032455},
                {lat: 44.431857, lng: 26.032343},
                {lat: 44.431578, lng: 26.032106},
                {lat: 44.431435, lng: 26.032109},
                {lat: 44.431007, lng: 26.032016},
                {lat: 44.430997, lng: 26.031917},
                {lat: 44.430911, lng: 26.031779},
                {lat: 44.431234, lng: 26.031665},
                {lat: 44.4319, lng: 26.031241},
                {lat: 44.433421, lng: 26.029705},
                {lat: 44.433769, lng: 26.029285},
                {lat: 44.43377, lng: 26.029284},
                {lat: 44.43377, lng: 26.029283},
                {lat: 44.43377, lng: 26.029283},
                {lat: 44.43377, lng: 26.029283},
                {lat: 44.43377, lng: 26.029283},
                {lat: 44.43377, lng: 26.029283},
                {lat: 44.43377, lng: 26.029283},
                {lat: 44.43377, lng: 26.029283},
                {lat: 44.433771, lng: 26.029283},
                {lat: 44.433771, lng: 26.029283},
                {lat: 44.433771, lng: 26.029283},
                {lat: 44.433771, lng: 26.029283},
                {lat: 44.433771, lng: 26.029283},
                {lat: 44.433771, lng: 26.029283},
                {lat: 44.43377, lng: 26.029283},
                {lat: 44.43377, lng: 26.029283},
                {lat: 44.433769, lng: 26.029283},
                {lat: 44.433769, lng: 26.029283},
                {lat: 44.433769, lng: 26.029283},
                {lat: 44.433769, lng: 26.029282},
                {lat: 44.433769, lng: 26.029283},
                {lat: 44.433769, lng: 26.029282},
                {lat: 44.433769, lng: 26.029282},
                {lat: 44.433769, lng: 26.029282},
                {lat: 44.433768, lng: 26.029282},
                {lat: 44.433768, lng: 26.029282},
                {lat: 44.433768, lng: 26.029282},
                {lat: 44.433768, lng: 26.029282},
                {lat: 44.433768, lng: 26.029282},
                {lat: 44.433768, lng: 26.029282},
                {lat: 44.433768, lng: 26.029282},
                {lat: 44.433768, lng: 26.029282},
                {lat: 44.433768, lng: 26.029282},
                {lat: 44.433768, lng: 26.029282},
                {lat: 44.433768, lng: 26.029281},
                {lat: 44.433767, lng: 26.029281},
                {lat: 44.433768, lng: 26.029281},
                {lat: 44.433767, lng: 26.029281},
                {lat: 44.433767, lng: 26.029281},
                {lat: 44.433767, lng: 26.029281},
                {lat: 44.433767, lng: 26.029281},
                {lat: 44.433768, lng: 26.02928},
                {lat: 44.433767, lng: 26.02928},
                {lat: 44.433766, lng: 26.02928},
                {lat: 44.433766, lng: 26.02928},
                {lat: 44.433765, lng: 26.02928},
                {lat: 44.433765, lng: 26.02928},
                {lat: 44.433765, lng: 26.02928},
                {lat: 44.433765, lng: 26.02928},
                {lat: 44.433765, lng: 26.02928},
                {lat: 44.433764, lng: 26.02928},
                {lat: 44.433764, lng: 26.02928},
                {lat: 44.433764, lng: 26.02928},
                {lat: 44.433764, lng: 26.02928},
                {lat: 44.433764, lng: 26.02928},
                {lat: 44.433764, lng: 26.02928},
                {lat: 44.433764, lng: 26.02928},
                {lat: 44.433764, lng: 26.02928},
                {lat: 44.433764, lng: 26.02928},
                {lat: 44.433764, lng: 26.02928},
                {lat: 44.433765, lng: 26.029279},
                {lat: 44.433767, lng: 26.029279},
                {lat: 44.433768, lng: 26.029279},
                {lat: 44.43377, lng: 26.029279},
                {lat: 44.43377, lng: 26.029279},
                {lat: 44.433775, lng: 26.029279},
                {lat: 44.433778, lng: 26.029279},
                {lat: 44.433778, lng: 26.029279},
                {lat: 44.433777, lng: 26.029279},
                {lat: 44.433777, lng: 26.029279},
                {lat: 44.433777, lng: 26.029278},
                {lat: 44.433777, lng: 26.029278},
                {lat: 44.433776, lng: 26.029278},
                {lat: 44.433775, lng: 26.029278},
                {lat: 44.433775, lng: 26.029278},
                {lat: 44.433775, lng: 26.029278},
                {lat: 44.433776, lng: 26.029278},
                {lat: 44.433779, lng: 26.029277},
                {lat: 44.433777, lng: 26.029278},
                {lat: 44.433778, lng: 26.029277},
                {lat: 44.433779, lng: 26.029279},
                {lat: 44.433796, lng: 26.029291},
                {lat: 44.433819, lng: 26.029305},
                {lat: 44.43385, lng: 26.029298},
                {lat: 44.43385, lng: 26.029298},
                {lat: 44.433851, lng: 26.029299},
                {lat: 44.433852, lng: 26.029299},
                {lat: 44.433852, lng: 26.029299},
                {lat: 44.433853, lng: 26.029299},
                {lat: 44.433854, lng: 26.0293},
                {lat: 44.433857, lng: 26.029309},
                {lat: 44.433855, lng: 26.029323},
                {lat: 44.433815, lng: 26.029342},
                {lat: 44.43377, lng: 26.029303},
                {lat: 44.43372, lng: 26.029252},
                {lat: 44.433667, lng: 26.029231},
                {lat: 44.433673, lng: 26.029287},
                {lat: 44.433696, lng: 26.029213},
                {lat: 44.433656, lng: 26.029168},
                {lat: 44.433812, lng: 26.029091},
                {lat: 44.433856, lng: 26.029159},
                {lat: 44.433886, lng: 26.029177},
                {lat: 44.433838, lng: 26.0292},
                {lat: 44.434044, lng: 26.029258},
                {lat: 44.433827, lng: 26.029341},
                {lat: 44.433596, lng: 26.029819},
                {lat: 44.434044, lng: 26.029869},
                {lat: 44.434038, lng: 26.029772},
                {lat: 44.434046, lng: 26.029659},
                {lat: 44.434056, lng: 26.029529},
                {lat: 44.433881, lng: 26.029401},
                {lat: 44.434142, lng: 26.029201},
                {lat: 44.434159, lng: 26.029064},
                {lat: 44.43394, lng: 26.028874},
                {lat: 44.433897, lng: 26.028956},
                {lat: 44.433907, lng: 26.029012},
                {lat: 44.43399, lng: 26.0291},
                {lat: 44.433951, lng: 26.029266},
                {lat: 44.433944, lng: 26.029284},
                {lat: 44.433944, lng: 26.029284},
                {lat: 44.433944, lng: 26.029284},
                {lat: 44.433943, lng: 26.029284},
                {lat: 44.433941, lng: 26.029284},
                {lat: 44.433939, lng: 26.029283},
                {lat: 44.43394, lng: 26.029282},
                {lat: 44.433939, lng: 26.029282},
                {lat: 44.43394, lng: 26.02928},
                {lat: 44.433939, lng: 26.029271},
                {lat: 44.433898, lng: 26.029257},
                {lat: 44.433868, lng: 26.02923},
                {lat: 44.433786, lng: 26.029239},
                {lat: 44.433937, lng: 26.029191},
                {lat: 44.433866, lng: 26.029157},
                {lat: 44.433736, lng: 26.029128},
                {lat: 44.433239, lng: 26.028605},
                {lat: 44.433239, lng: 26.028605},
                {lat: 44.433238, lng: 26.028605},
                {lat: 44.433238, lng: 26.028605},
                {lat: 44.433238, lng: 26.028605},
                {lat: 44.433237, lng: 26.028605},
                {lat: 44.433237, lng: 26.028606},
                {lat: 44.433237, lng: 26.028605},
                {lat: 44.433237, lng: 26.028605},
                {lat: 44.433237, lng: 26.028605},
                {lat: 44.433237, lng: 26.028605},
                {lat: 44.433237, lng: 26.028605},
                {lat: 44.433237, lng: 26.028605},
                {lat: 44.433237, lng: 26.028605},
                {lat: 44.433237, lng: 26.028605},
                {lat: 44.433237, lng: 26.028605},
                {lat: 44.433237, lng: 26.028605},
                {lat: 44.433239, lng: 26.028606},
                {lat: 44.43324, lng: 26.028606},
                {lat: 44.433241, lng: 26.028607},
                {lat: 44.433241, lng: 26.028608},
                {lat: 44.433241, lng: 26.028608},
                {lat: 44.433242, lng: 26.028609},
                {lat: 44.433242, lng: 26.028609},
                {lat: 44.433242, lng: 26.028609},
                {lat: 44.433243, lng: 26.02861},
                {lat: 44.433243, lng: 26.02861},
                {lat: 44.433244, lng: 26.028611},
                {lat: 44.433244, lng: 26.028611},
                {lat: 44.433244, lng: 26.028612},
                {lat: 44.433244, lng: 26.028612},
                {lat: 44.433269, lng: 26.028636},
                {lat: 44.433256, lng: 26.028627},
                {lat: 44.433241, lng: 26.028614},
                {lat: 44.433239, lng: 26.028611},
                {lat: 44.433239, lng: 26.028611},
                {lat: 44.433238, lng: 26.028611},
                {lat: 44.433238, lng: 26.028611},
                {lat: 44.433238, lng: 26.028611},
                {lat: 44.433238, lng: 26.028611},
                {lat: 44.433238, lng: 26.028611},
                {lat: 44.433238, lng: 26.028611},
                {lat: 44.433238, lng: 26.028611},
                {lat: 44.433238, lng: 26.028611},
                {lat: 44.433238, lng: 26.028611},
                {lat: 44.433237, lng: 26.028611},
                {lat: 44.433237, lng: 26.028611},
                {lat: 44.433237, lng: 26.028611},
                {lat: 44.433237, lng: 26.028611},
                {lat: 44.433237, lng: 26.028611},
                {lat: 44.433237, lng: 26.028611},
                {lat: 44.433237, lng: 26.028611},
                {lat: 44.433237, lng: 26.028611},
                {lat: 44.433237, lng: 26.028611},
                {lat: 44.433237, lng: 26.028611},
                {lat: 44.433237, lng: 26.028611},
                {lat: 44.433237, lng: 26.028611},
                {lat: 44.433237, lng: 26.028611},
                {lat: 44.433237, lng: 26.028611},
                {lat: 44.433237, lng: 26.028611},
                {lat: 44.433237, lng: 26.028611},
                {lat: 44.433237, lng: 26.02861},
                {lat: 44.433237, lng: 26.02861},
                {lat: 44.433237, lng: 26.02861},
                {lat: 44.433237, lng: 26.02861},
                {lat: 44.433237, lng: 26.02861},
                {lat: 44.433237, lng: 26.02861},
                {lat: 44.433237, lng: 26.02861},
                {lat: 44.433237, lng: 26.02861},
                {lat: 44.433236, lng: 26.02861},
                {lat: 44.433236, lng: 26.02861},
                {lat: 44.433236, lng: 26.02861},
                {lat: 44.433236, lng: 26.02861},
                {lat: 44.433236, lng: 26.02861},
                {lat: 44.433236, lng: 26.028609},
                {lat: 44.433236, lng: 26.028609},
                {lat: 44.433236, lng: 26.028609},
                {lat: 44.433236, lng: 26.028609},
                {lat: 44.433236, lng: 26.028609},
                {lat: 44.433236, lng: 26.028609},
                {lat: 44.433236, lng: 26.028609},
                {lat: 44.433236, lng: 26.028609},
                {lat: 44.433236, lng: 26.028609},
                {lat: 44.433236, lng: 26.028609},
                {lat: 44.433236, lng: 26.028609},
                {lat: 44.433235, lng: 26.028609},
                {lat: 44.433235, lng: 26.028609},
                {lat: 44.433235, lng: 26.028609},
                {lat: 44.433235, lng: 26.028609},
                {lat: 44.433235, lng: 26.028609},
                {lat: 44.433235, lng: 26.028608},
                {lat: 44.433235, lng: 26.028608},
                {lat: 44.433235, lng: 26.028608},
                {lat: 44.433234, lng: 26.028608},
                {lat: 44.433234, lng: 26.028608},
                {lat: 44.433234, lng: 26.028608},
                {lat: 44.433234, lng: 26.028608},
                {lat: 44.433234, lng: 26.028608},
                {lat: 44.433233, lng: 26.028608},
                {lat: 44.433227, lng: 26.028607},
                {lat: 44.433218, lng: 26.028607},
                {lat: 44.433218, lng: 26.028607},
                {lat: 44.433217, lng: 26.028607},
                {lat: 44.433213, lng: 26.02861},
                {lat: 44.433219, lng: 26.028614},
                {lat: 44.433219, lng: 26.028614},
                {lat: 44.433235, lng: 26.028624},
                {lat: 44.433438, lng: 26.028619},
                {lat: 44.433236, lng: 26.028588},
                {lat: 44.433153, lng: 26.028518},
                {lat: 44.433145, lng: 26.028517},
                {lat: 44.43314, lng: 26.028516},
                {lat: 44.43314, lng: 26.028516},
                {lat: 44.43314, lng: 26.028516},
                {lat: 44.433139, lng: 26.028515},
                {lat: 44.433164, lng: 26.028527},
                {lat: 44.433183, lng: 26.028531},
                {lat: 44.433201, lng: 26.028542},
                {lat: 44.43317, lng: 26.028538},
                {lat: 44.433194, lng: 26.028523},
                {lat: 44.433432, lng: 26.028833},
                {lat: 44.433432, lng: 26.028833},
                {lat: 44.433432, lng: 26.028833},
                {lat: 44.433432, lng: 26.028833},
                {lat: 44.433432, lng: 26.028833},
                {lat: 44.433432, lng: 26.028833},
                {lat: 44.433432, lng: 26.028833},
                {lat: 44.433432, lng: 26.028833},
            ];
            const flightPath = new this.maps.Polyline({
                path: flightPlanCoordinates,
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2,
            });
            // flightPath.setMap(this.map);
        }

        if (navigator.geolocation) {
            this.startGeolocation();
        }
    }
    @boundMethod
    startGeolocation() {
        this.failedGeolocation = 0;

        if (this.geolocationWatcher) {
            navigator.geolocation.clearWatch(this.geolocationWatcher);
        }
        this.geolocationWatcher = navigator.geolocation.watchPosition(this.handlePositionUpdate, (e) => {
            this.failedGeolocation += 1;

            if (this.failedGeolocation > 10) {
                navigator.geolocation.clearWatch(this.geolocationWatcher);
            }

            if (this.mounted) {
                this.setState({
                    geolocation: 'error',
                });
            }
        }, {
            enableHighAccuracy: true,
            maximumAge: 10000,
        });
        // try to force a position read
        navigator.geolocation.getCurrentPosition(this.handlePositionUpdate, (e) => {
            this.failedGeolocation += 1;

            if (this.mounted) {
                this.setState({
                    geolocation: 'error',
                });
            }
        }, {
            enableHighAccuracy: true,
            maximumAge: 10000,
        });

        this.setState({
            geolocation: 'starting',
        });
    }
    @boundMethod
    onMarkerClick(key) {
        if (!key) {
            return;
        }

        const clickedScooter = this.props.scooters?.list?.find(scooter => scooter.dbID === key);

        if (!clickedScooter.location.latitude || !clickedScooter.location.longitude) {
            return;
        }
        this.userInteractedWithMap = true;

        this.setState({
            scooterOverlay: true,
            selectedScooterID: key,
        }, () => {
            const scooterPositionInPixels = this.latLng2Point(new this.maps.LatLng(
                clickedScooter.location.latitude,
                clickedScooter.location.longitude,
            ));

            const scooterOffsetPosition = this.point2LatLng(new this.maps.Point(scooterPositionInPixels.x + ScooterMarker.SIZE / 2, scooterPositionInPixels.y + 150));

            this.map?.panTo(scooterOffsetPosition);
        });
    }
    @boundMethod
    zoomToFit(forced) {
        if (this.userInteractedWithMap && !forced) {
            // if the user moved the map, dont autozoom
            return;
        }

        const bounds = this.getMapBounds(this.props.scooters.list);
        if (this.state.userPosition) {
            bounds.extend(new this.maps.LatLng(this.state.userPosition.coords?.latitude, this.state.userPosition.coords?.longitude));
        }

        const originalMaxZoom = this.map.maxZoom;
        this.map.setOptions({maxZoom: GoogleMaps.MIN_AUTO_ZOOM});
        this.map.fitBounds(bounds);
        this.map.setOptions({maxZoom: originalMaxZoom});
    }

    componentDidUpdate(prevProps, prevState) {
        const scooterKeys = (this.props.scooters?.list || []).map(scooter => scooter.dbID).join();

        if (this.map && scooterKeys && scooterKeys !== this.scooterKeys) {
            this.scooterKeys = scooterKeys;
            this.zoomToFit();
            this.userInteractedWithMap = true;
        }
        if (this.props.server?.serverTimestamp !== prevProps.server?.serverTimestamp) {
            this.timeDiffToServer = Date.now() / 1000 - this.props.server?.serverTimestamp;
        }
        if (prevProps.scooters?.list !== this.props.scooters?.list) {
            if (this.animationStopped) {
                window.requestAnimationFrame(this.animationFrame);
            }
        }
    }
    @boundMethod
    onInteraction() {
        this.userInteractedWithMap = true;
    }
    @boundMethod
    onMapClick() {
        this.setState({
            selectedScooterID: undefined,
            scooterOverlay: false,
        });
    }
    @boundMethod
    animationFrame() {
        let finishedAnimation = true;

        const animation = {};
        this.props.scooters?.list?.forEach(scooter => {
            if (!scooter?.dbID) {
                return;
            }

            const step = this.state.animation[scooter.dbID]?.start ? Math.round((Date.now() - this.state.animation[scooter.dbID]?.start) * TARGET_FPS * MARKER_TRANSITION_DURATION / (MARKER_TRANSITION_DURATION * 1000)) : 0;
            if (step >= GoogleMaps.STEPS) {
                animation[scooter.dbID] = {
                    latitude: scooter.location?.latitude,
                    longitude: scooter.location?.longitude,
                };
                return;
            }

            finishedAnimation = false;
            const currentLatitude = this.state.animation[scooter.dbID]?.latitude || scooter.location?.latitude;
            const currentLongitude = this.state.animation[scooter.dbID]?.longitude || scooter.location?.longitude;

            const i = ease[step];
            animation[scooter.dbID] = {
                start: this.state.animation[scooter.dbID]?.start || Date.now(),
                latitude: (1 - i) * currentLatitude + i * scooter.location?.latitude,
                longitude: (1 - i) * currentLongitude + i * scooter.location?.longitude,
                step: step + 1,
            };
        });

        this.setState({
            animation,
        });

        if (finishedAnimation) {
            this.animationStopped = true;

            if (this.state.selectedScooterID) {
                const scooterPositionInPixels = this.latLng2Point(new this.maps.LatLng(
                    animation[this.state.selectedScooterID].latitude,
                    animation[this.state.selectedScooterID].longitude,
                ));

                const scooterOffsetPosition = this.point2LatLng(new this.maps.Point(scooterPositionInPixels.x + ScooterMarker.SIZE / 2, scooterPositionInPixels.y + 150));

                this.map?.panTo(scooterOffsetPosition);
            }
        }
        else {
            this.animationStopped = false;
            window.requestAnimationFrame(this.animationFrame);
        }
    }
    componentDidMount() {
        if (this.props.server?.serverTimestamp) {
            this.timeDiffToServer = Date.now() / 1000 - this.props.server?.serverTimestamp;
        }

        this.locationSubscription = requestSubscription(this.props.relay.environment, {
            subscription: GoogleMaps.subscription,
            variables: {},
            onError: (error) => {
                console.log(error, 'error');
            },
            updater: (store, t) => {
                const payload = store.getRootField('scooterLocationUpdate');
                console.log(t, t?.status);

                const scooterList = store.getRoot().getLinkedRecord('viewer').getLinkedRecord('scooters').getLinkedRecords('list');
                scooterList.forEach(scooter => {
                    if (scooter.getValue('dbID') === payload.getValue('scooterID')) {
                        if (payload.getLinkedRecord('location').getValue('latitude') !== 0) {
                            scooter.setLinkedRecord(payload.getLinkedRecord('location'), 'location');
                            scooter.setLinkedRecord(payload.getLinkedRecord('info'), 'info');
                        }
                        if (payload.getValue('status')) {
                            scooter.setValue(payload.getValue('status'), 'status');
                        }
                    }
                });
            },
        });
        this.animationFrame();
        this.mounted = true;
    }
    componentWillUnmount() {
        this.mounted = false;
        this.locationSubscription?.dispose();
    }
    @boundMethod
    lockScooter() {
        if (this.state.locking || !this.state.selectedScooterID) {
            return false;
        }

        const scooterID = this.state.selectedScooterID;

        this.setState({
            locking: true,
        });
        const status = this.props.scooters?.list?.find(scooter => scooter.dbID === scooterID)?.status;
        if (status === 'locked' || status === 'stolen') {
            commitMutation(this.props.relay.environment, {
                mutation: graphql`
                    mutation mapUnlockScooterMutation($scooterID: ID) {
                        unlockScooter(scooterID: $scooterID) {
                            done
                        }
                    }
                `,
                variables: {
                    scooterID,
                },
                onError(err) {
                    console.error(err);
                },
                updater: (store) => {
                    const payload = store.getRootField('unlockScooter');
                    if (payload.getValue('done')) {
                        const scooterList = store.getRoot().getLinkedRecord('viewer').getLinkedRecord('scooters').getLinkedRecords('list');
                        scooterList.forEach(scooter => {
                            if (scooter.getValue('dbID') === scooterID) {
                                scooter.setValue('active', 'status');
                            }
                        });
                    }

                    this.setState({
                        locking: false,
                    });
                },
            });
        }
        else {
            commitMutation(this.props.relay.environment, {
                mutation: graphql`
                mutation mapLockScooterMutation($scooterID: ID) {
                    lockScooter(scooterID: $scooterID) {
                        smsSent
                    }
                }
            `,
                variables: {
                    scooterID,
                },
                onError(err) {
                    console.error(err);
                },
                updater: (store) => {
                    const payload = store.getRootField('lockScooter');
                    if (payload.getValue('smsSent')) {
                        const scooterList = store.getRoot().getLinkedRecord('viewer').getLinkedRecord('scooters').getLinkedRecords('list');
                        scooterList.forEach(scooter => {
                            if (scooter.getValue('dbID') === scooterID) {
                                scooter.setValue('stolen', 'status');
                            }
                        });
                    }

                    this.setState({
                        locking: false,
                    });
                },
            });
        }
    }
    render() {
        const {
            selectedScooterID,
        } = this.state;
        const selectedScooter = this.props.scooters?.list?.find(scooter => scooter?.dbID === selectedScooterID);

        if (this.myPositionButton) {
            if (this.state.geolocation === 'active') {
                this.myPositionButton.className = classnames(classes['Map__myLocation'], classes['Map__myLocation--active']);
            }
            else if (this.state.geolocation === 'starting') {
                this.myPositionButton.className = classnames(classes['Map__myLocation'], classes['Map__myLocation--active']);
            }
            else if (this.state.geolocation === 'error') {
                this.myPositionButton.className = classnames(classes['Map__myLocation'], classes['Map__myLocation--error']);
            }
            else {
                this.myPositionButton.className = classes['Map__myLocation'];
            }
        }
        let lastScooterUpdate = (selectedScooter?.location?.serverTimestamp > selectedScooter?.info?.serverTimestamp ? selectedScooter?.location?.serverTimestamp : selectedScooter?.info?.serverTimestamp) + this.timeDiffToServer;

        return (
            <div className={classes['Map']}>
                <GoogleMapReact
                    bootstrapURLKeys={{key: 'AIzaSyAcIYrLFmfnCVMWhntE_q5w3kkd7JLXa38'}}
                    defaultCenter={{
                        lat: 44.4271913,
                        lng: 26.1016447,
                    }}
                    defaultZoom={12}
                    options={{
                        gestureHandling: 'greedy',
                        disableDefaultUI: true,
                        zoomControl: true,
                        scaleControl: true,
                        rotateControl: true,
                        clickableIcons: false,
                    }}
                    yesIWantToUseGoogleMapApiInternals={true}
                    onGoogleApiLoaded={this.mapLoaded}
                    hoverDistance={70}
                    distanceToMouse={GoogleMaps.distanceToMouse}
                    onChildClick={this.onMarkerClick}
                    onDrag={this.onInteraction}
                    onClick={this.onMapClick}
                >
                    {this.state.userPosition ? <MyPositionMarker
                        lat={this.state.userPosition.coords?.latitude}
                        lng={this.state.userPosition.coords?.longitude}
                    /> : null}
                    {
                        this.props.scooters?.list?.map(scooter => scooter?.location && <ScooterMarker
                            key={scooter.dbID}
                            map={this.map}
                            maps={this.maps}
                            lat={this.state.animation[scooter?.dbID]?.latitude || scooter?.location?.latitude}
                            lng={this.state.animation[scooter?.dbID]?.longitude || scooter?.location?.longitude}
                            accuracy={50}
                            size={{width: 70, height: 70}}
                            color={scooter?.status === 'locked' ? 'orange' : scooter?.status === 'stolen' ? 'red' : 'green'}
                            battery={scooter?.info?.batteryLevel}
                        />)
                    }
                </GoogleMapReact>
                <div className={classnames(classes['Map__scooterOverlay'], {
                    [classes['Map__scooterOverlay--active']]: this.state.scooterOverlay,
                })}>
                    <div className={classes['Map__scooterOverlay__iconBackground']}></div>
                    <div className={classes['Map__scooterOverlay__icon']}></div>
                    <div className={classes['Map__scooterOverlay__content']}>
                        <div className={classes['Map__scooterOverlay__entry']}>
                            <div className={classes['Map__scooterOverlay__label']}>Battery:</div>
                            <div className={classes['Map__scooterOverlay__value']}>{selectedScooter?.info?.batteryLevel}%</div>
                        </div>
                        <div className={classes['Map__scooterOverlay__entry']}>
                            <div className={classes['Map__scooterOverlay__label']}>Current speed:</div>
                            <div className={classes['Map__scooterOverlay__value']}>{selectedScooter?.info?.scooterSpeed}km/h</div>
                        </div>
                        <div className={classes['Map__scooterOverlay__entry']}>
                            <div className={classes['Map__scooterOverlay__label']}>Last update:</div>
                            <div className={classes['Map__scooterOverlay__value']}>{humanize.relativeTime(lastScooterUpdate)}</div>
                        </div>
                        {
                            (selectedScooter?.status === 'stolen' || selectedScooter?.status === 'locked') ? (
                                <Button flat className={classnames(classes['Map__scooterOverlay__lock'], classes['Map__scooterOverlay__lock--unlock'])} onClick={this.lockScooter} disabled={this.state.locking}>Unlock scooter</Button>
                            ) : (
                                <Button flat className={classes['Map__scooterOverlay__lock']} onClick={this.lockScooter} disabled={this.state.locking}>Block scooter</Button>
                            )
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default createFragmentContainer(
    GoogleMaps,
    {
        scooters: graphql`
        fragment map_scooters on Scooters {
            list {
                dbID
                name
                status
                location {
                    serverTimestamp
                    latitude
                    longitude
                    hdop
                }
                info {
                    serverTimestamp
                    batteryLevel
                    adseroVoltage
                    scooterSpeed
                }
            }
        }
        `,
        server: graphql`
        fragment map_server on Server {
            serverTimestamp
        }
        `,
    },
);