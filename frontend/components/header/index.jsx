import PropTypes from 'prop-types';
import React from 'react';
import {Button} from 'material-react';
import classnames from 'classnames';
import {boundMethod} from 'autobind-decorator';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {commitMutation, graphql} from 'react-relay';

import {library} from '@fortawesome/fontawesome-svg-core';
import {faBars} from '@fortawesome/pro-regular-svg-icons';
import {faGoogle, faFacebookF} from '@fortawesome/free-brands-svg-icons';
library.add(faBars, faGoogle, faFacebookF);

import logo from '../../images/logo-adsero.png';

import classes from './index.module.scss';

function urlBase64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/-/g, '+')
        .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}
function askNotificationPermission() {
    return new Promise((resolve, reject) => {
        const permissionResult = Notification.requestPermission((result) => {
            resolve(result);
        });

        if (permissionResult) {
            permissionResult.then(resolve, reject);
        }
    });
}

class Header extends React.Component {
    static propTypes = {
        location: PropTypes.object,
        user: PropTypes.object,
    }
    static registerServiceUUID = 0x181C;
    static dbIDUUID = 0x2AC3;
    static registerStatusUUID = 0x2AC4;

    state = {
        menu: false,
    };
    @boundMethod
    openMenu() {
        this.setState({
            menu: true,
        });
    }
    @boundMethod
    closeMenu() {
        this.setState({
            menu: false,
        });
    }
    @boundMethod
    async addDevice(e) {
        e?.preventDefault();

        this.setState({
            waitingForDevice: true,
        });
        const device = await navigator.bluetooth.requestDevice({
            filters: [
                {namePrefix: 'Adse.ro'},
            ],
            optionalServices: [
                Header.registerServiceUUID,
                Header.dbIDUUID,
                Header.registerStatusUUID,
            ],
        });
        this.setState({
            waitingForDevice: false,
        });

        const server = await device.gatt.connect();
        const service = await server.getPrimaryService(Header.registerServiceUUID);
        const dbIDChr = await service.getCharacteristic(Header.dbIDUUID);
        const registerStatusChr = await service.getCharacteristic(Header.registerStatusUUID);

        if (registerStatusChr.properties.notify) {
            registerStatusChr.addEventListener('characteristicvaluechanged', (event) => {
                const registerStatus = event.target.value.getUint32(0, true);

                if (registerStatus > 3) {
                    location.reload();
                }
            });
        }

        await registerStatusChr.startNotifications();
        await dbIDChr.writeValue(new Int32Array([this.props.user?.dbID]));
    }
    @boundMethod
    async enableNotifications() {
        try {
            const registration = await navigator.serviceWorker.register('/service-worker.js');

            const permissionResult = await askNotificationPermission();
            if (permissionResult !== 'granted') {
                throw permissionResult;
            }

            const subscribeOptions = {
                userVisibleOnly: true,
                applicationServerKey: urlBase64ToUint8Array('BA0EerI9NwBV2bFNcYtDStZLS9N9OoGXfl5JCEuoXIq4alSPLkciy6HlOcW3FqB9WdW5OKFXfHdscAZNxC38XoM'),
            };

            const PushSubscription = await registration.pushManager.subscribe(subscribeOptions);

            commitMutation(window.environment, {
                mutation: graphql`
                    mutation headerUserWebPushMutation($input: String) {
                        userWebPush(input: $input) {
                            done
                        }
                    }
                `,
                variables: {
                    input: JSON.stringify(PushSubscription),
                },
                onError(err) {
                    console.error(err);
                },
                onCompleted: (response) => {
                    this.setState({
                        grantedPermission: response.done,
                        menu: false,
                    });
                },
            });
        }
        catch (err) {
            console.error(err);
        }
    }
    render() {
        let photo;
        const user = this.props.user;
        if (user?.photo?.includes('http')) {
            photo = user.photo;
        }
        else {
            photo = '/static/' + user?.photo;
        }

        return (
            <div className={classnames(classes['Header'], {
                [classes['Header--active']]: this.state.menu,
            })}>
                <div className={classes['Header__burger']}>
                    <Button raised onClick={this.openMenu}>
                        <FontAwesomeIcon icon={['far', 'bars']} />
                    </Button>
                </div>
                <nav className={classes['Header__nav']}>
                    <div className={classes['Header__logo']} onClick={this.closeMenu}><img src={logo} /></div>
                    {
                        user ? (<div className={classes['Header__list']}>
                            <Button flat onClick={this.addDevice}>Add new device</Button>
                            {Notification?.permission === 'granted' || this.state.grantedPermission ? null : <Button flat onClick={this.enableNotifications}>Activate notifications</Button>}
                        </div>
                        ) : null
                    }
                    <div className={classes['Header__login']}>
                        {
                            user ? (
                                <>
                                    <a className={classes['Header__socialLink']} href="/auth/logout" ><Button className={classes['Header__facebook']} flat><span className="text">Logout</span></Button></a>
                                </>
                            ) : (
                                <>
                                    <div className={classes['Header__socialTitle']}>Login with</div>
                                    <a className={classnames(classes['Header__socialLink'], classes['Header__socialLink--facebook'])} href="/auth/facebook"><Button className={classes['Header__facebook']} flat><FontAwesomeIcon fixedWidth icon={['fab', 'facebook-f']} /><span className="text">Facebook</span></Button></a>
                                    <div className={classes['Header__socialDelimiter']}><span>OR</span></div>
                                    <a className={classnames(classes['Header__socialLink'], classes['Header__socialLink--google'])} href="/auth/google"><Button className={classes['Header__google']} flat><FontAwesomeIcon fixedWidth icon={['fab', 'google']} /><span className="text">Google</span></Button></a>
                                </>
                            )
                        }
                    </div>
                </nav>
                <Button ripple={false} className={classes['Header__backdrop']} onClick={this.closeMenu} />
            </div>
        );
    }
}


export default Header;