import React from 'react';
import PropTypes from 'prop-types';
import {createRefetchContainer, graphql} from 'react-relay';
import {Actions as FarceActions} from 'farce';
import classnames from 'classnames';

import environment from './environment';
// import RegisterMutation from './mutations/RegisterMutation';
// import SendResetPasswordMailMutation from './mutations/SendResetPasswordMailMutation';
// import Login from './views/login';
// import PasswordRecovery from './views/recoverPassword';
import LoginView from './views/login';

import './scss/common.scss';

class AppComponent extends React.Component {
    static guestUser = { }

    static propTypes = {
        relay: PropTypes.object.isRequired,
        children: PropTypes.node,
        viewer: PropTypes.shape({
            user: PropTypes.shape({
                dbID: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
                name: PropTypes.string,
                photo: PropTypes.string,
                status: PropTypes.string.isRequired,
            }),
        }),
    }
    static UserQuery = graphql`
        query app_fetchQuery {
            viewer {
                ...app_viewer
            }
        }
    `

    state = {};
    render() {
        const user = this.props.viewer?.user;

        if (user && user.status === 'disabled') {
            return (
                <h1>Banned user!</h1>
            );
        }
        return this.props.children;
    }
}



export const AppQuery = AppComponent.UserQuery;
export const App = createRefetchContainer(
    AppComponent,
    {
        viewer: graphql`
            fragment app_viewer on Viewer {
                user {
                    dbID
                    photo
                    name
                    status
                }
            }
        `,
    },
    graphql`
        query app_refetchQuery {
            viewer {
                ...app_viewer
            }
        }
    `,
) ;