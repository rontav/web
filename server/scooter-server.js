const log = require('./utils/minilog')('Scooter server ');

const config = require('./config');
const fs = require('fs-extra');
const path = require('path');
const {
    Source,
    parse,
    validate,
    execute,
    specifiedRules,
} = require('graphql');
const readline = require('readline');
const camelcase = require('camelcase');

const mysql = require('./utils/mysql');
const apiSchema = require('./router/api');

function hexdump(input) {
    let temp = input.toString('hex');
    let temp2 = [];

    let i = 0;
    while (temp.length) {
        temp2.push(temp.substr(0, 2) + ' ');
        temp = temp.substr(2);
        i++;

        if (i === 8 || i === 16) {
            temp2.push(' ');
        }
        if (i === 16) {
            temp2.push('\n');
            i = 0;
        }
    }
    return temp2.join('');
}

function parseCStruct(input) {
    return input
        .split('\n')
        .filter(line => line && line.includes(';') && !line.includes('}'))
        .map(line => line.trim())
        .map(line => {
            const variableType = line.substr(0, line.indexOf(' '));
            const variables = line.substr(line.indexOf(' ') + 1).split(',');

            let output = [];
            for (let i = 0; i < variables.length; i++) {
                output.push([variables[i].trim().replace(/;/g, ''), variableType]);
            }
            return output;
        }).flat(1);
}

const typeSize = {
    'double': 8,
    'float': 4,
    'uint32_t': 4,
    'uint16_t': 2,
    'uint8_t': 1,
};

function parseFromBuffer(buffer, variables, variableIndex) {
    let offset = 1;
    for (let i = 0; i < variableIndex; i++) {
        offset += typeSize[variables[i][1]];
    }

    const type = variables[variableIndex][1];
    let bufferFunction;
    if (type === 'double') {
        bufferFunction = buffer.readDoubleLE;
    }
    else if (type === 'float') {
        bufferFunction = buffer.readFloatLE;
    }
    else if (type === 'uint32_t') {
        bufferFunction = buffer.readInt32LE;
    }
    else if (type === 'uint16_t') {
        bufferFunction = buffer.readInt16LE;
    }
    else if (type === 'uint8_t') {
        bufferFunction = buffer.readInt8;
    }

    return bufferFunction.call(buffer, offset);
}

const mutationMap = {
    'L': {
        mutation: `
            mutation location($scooterID: ID, $input: LocationAndStatusInput) {
                updateLocationAndStatus(scooterID: $scooterID, input: $input) {
                    done
                }
            }
        `,
        variables: parseCStruct(`
        uint8_t write_adsero_status;
        typedef struct {
            double scooter_timestamp;
            double latitude, longitude;
            float altitude;
            float speed;
            float heading;
            float hdop, pdop, vdop;
            uint8_t satellites_in_view;
            uint8_t satellites_used;
            uint8_t cn0_max;
            uint8_t fix;
        } Location;
        typedef struct {
            float adsero_voltage;
            float adsero_temperature;
            float scooter_speed;
            uint8_t battery_level;
            uint8_t power_good;
            uint8_t location_status;
            uint8_t alarm_status;
            uint8_t dashboard_status;
            uint8_t scooter_status;
            uint8_t adsero_status;
        } Scooter_info;
        `),
        ignoredVariables: ['fix'],
    },
    'R': {
        mutation: `
            mutation registerScooter($scooterID: ID, $input: RegisterScooterInput) {
                registerScooter(scooterID: $scooterID, input: $input) {
                    done
                }
            }
        `,
        variables: parseCStruct(`
            uint32_t userID;
        `),
    },
    'S': {
        mutation: `
            query scooterStatus($scooterID: ID) {
                viewer {
                    scooters {
                        status(scooterID: $scooterID)
                    }
                }
            }
        `,
        variables: [],
        sendResponse: true,
        processResponse: data => {
            const status = data?.viewer?.scooters?.status?.toUpperCase();

            const struct = `
            typedef enum {
                DISABLED = 0,
                NOT_REGISTERED,
                STOLEN,
                LOCKED,
                ACTIVE,
            } Adsero_status;
            `.split('\n')
                .filter(line => line && line.includes(','))
                .map(line => line.trim().split(' ')[0].replace(/,/g, ''));

            return struct.indexOf(status).toString();
        },
    },
};

function deserializeVariables(buffer, variables, ignoredVariables) {
    let k = -1;
    const ret = {input: {}};

    for (let [variableName] of variables) {
        k++;
        if ((ignoredVariables || []).includes(variableName)) {
            continue;
        }
        ret.input[camelcase(variableName)] = parseFromBuffer(buffer, variables, k);
    }

    return ret;
}

const tcpServer = require('tls').createServer({
    cert: fs.readFileSync(path.resolve(__dirname, '../adsero-server.pem')),
    key: fs.readFileSync(path.resolve(__dirname, '../adsero-server.key')),
    passphrase: config.scooters.caPassword,
    ca: [fs.readFileSync(path.resolve(__dirname, '../ca-cert.pem'))],
    requestCert: true,
    rejectUnauthorized: true,
    minVersion: 'TLSv1.2',
});
tcpServer.maxConnections = 1000;
tcpServer.listen(config.ports.scooters, () => {
    log.info('Scooter TCP Server listening on socket ::', config.ports.scooters);
});

tcpServer.on('secureConnection', async (socket) => {
    if (!socket.authorized) {
        return socket.destroy();
    }

    const publicKey = socket.getCertificate().pubkey.toString('base64');
    const [[scooter]] = await mysql.query('SELECT dbID FROM Scooters WHERE publicKey = ? AND status != "disabled"', [publicKey]);
    socket.dbID = scooter?.dbID;

    if (!socket.dbID) {
        console.log('no scooter found', publicKey);
        return socket.destroy();
    }

    // set socket timeout to 24h for authenticated sockets
    socket.setTimeout(24 * 60 * 60 * 1000);

    log.info('New secure scooter', socket.dbID, socket.remoteAddress);

    socket.lineStream = readline.createInterface(socket);
    socket.lineStream.on('line', async (line) => {
        socket.write('ack\n');

        const buffer = Buffer.from(line, 'base64');
        const mvPair = mutationMap[buffer.toString()[0]];

        console.log('Packet received from client:');
        console.log(hexdump(buffer));

        if (!mvPair) {
            return;
        }

        // console.log(mvPair.mutation, mvPair.variables);

        try {
            let documentAST = parse(new Source(mvPair.mutation, 'GraphQL request'));
            const validationErrors = validate(apiSchema, documentAST, specifiedRules);

            if (validationErrors.length > 0) {
                throw validationErrors;
            }

            const {data, errors} = await execute({
                schema: apiSchema,
                document: documentAST,
                contextValue: {
                    isScooter: true,
                },
                variableValues: {
                    scooterID: socket.dbID,
                    ...deserializeVariables(buffer, mvPair.variables, mvPair.ignoredVariables),
                },
            });

            if (mvPair.sendResponse) {
                socket.write(mvPair.processResponse(data) + '\n');
            }

            if (errors) {
                throw errors;
            }
        }
        catch (error) {
            console.error(error);
        }
    });

    socket.on('timeout', () => {
        console.log('Socket timed out!', socket.dbID);
        socket.end();
        socket.destroy();
    });

    socket.on('error', (err) => {
        console.log(`Error: ${err}`);
    });
});
