const fs = require('fs');
const path = require('path');
const extend = require('extend');

var defaults = {
    appName: 'Adse.ro',
    production: false,
    publicPath: path.resolve(__dirname, '../frontend'),
    domain: 'adse.ro',
    ports: {
        scooters: 4444,
        web: 8080,
        webpack: 5000,
    },
    https: true, // not handled by nodejs
    mysql: {
        database: 'database-name',
        host: 'localhost',
        user: 'root',
        password: 'root',
    },
    nexmo: {
        apiKey: 'api-key',
        apiSecret: 'api-secret',
    },
    staticPath: path.resolve(__dirname, './static'),
    facebookAuth: {
        clientID: 'client-id',
        clientSecret: 'client-secret',
        callbackURL: '/auth/facebook/callback',
    },
    googleAuth: {
        clientID: 'client-id',
        clientSecret: 'client-secret',
        callbackURL: '/auth/google/callback',
    },
    scooters: {
        caPassword: 'secret',
        caPath: path.resolve(__dirname, '../adsero-server.key'),
    },
    vapid: {
        publicKey: 'publickey',
        privateKey: 'privatekey',
    },

    secret: 'secret',
    staticServerPath: 'server/static/server',
    certificatePath: 'certificate/',
    debug: {},
    resetPassword: {
        algorithm: 'aes-256-ctr',
        secret: 'terces',
    },
    recaptcha: {
        siteKey: 'site-key',
        secretKey: 'secret-key',
    },
    emails: {
        admins: ['mrvali97@gmail.com'],
    },
};

let config = defaults;
const configPath = path.resolve(__dirname, './config.json');

let fileFound = true;
try {
    fs.accessSync(configPath);
}
catch (e) {
    fileFound = false;
}
if (fileFound) {
    try {
        let jsonConfig = require(configPath);
        extend(true, config, jsonConfig);
    }
    catch (e) {
        console.error('config.json file is invalid');
    }
}

if (!config.production) {
    config.host = `${config.https ? 'https' : 'http'}://${config.domain}:${config.ports.web}`;
}
config.host = 'https://adse.ro';
config.facebookAuth.callbackURL = `${config.host}${config.facebookAuth.callbackURL}`;
config.googleAuth.callbackURL = `${config.host}${config.googleAuth.callbackURL}`;

module.exports = config;