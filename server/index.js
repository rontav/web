const log = require('./utils/minilog')('Express ');

const config = require('./config');

const {passport, authRouter} = require('./passport');
const apiSchema = require('./router/api');

const mysql = require('./utils/mysql');
const session = require('express-session');
const MySQLStore = require('express-mysql-session')(session);

const sessionStore = new MySQLStore({}, mysql);
const expressSession = session({
    passport,
    secret: config.secret,
    resave: false,
    saveUninitialized: false,
    store: sessionStore,
});

const cors = require('cors');
const {graphqlHTTP} = require('express-graphql');
const {supergraphiqlExpress} = require('super-graphiql-express');
const {SubscriptionServer} = require('subscriptions-transport-ws');
const {execute, subscribe} = require('graphql');
const WSServer = require('ws').Server;
const express = require('express');
const app = express();

if (config.production) {
    const helmet = require('helmet');
    const csp = require('helmet-csp');

    app.use(helmet());
    app.use(csp({
        directives: {
            upgradeInsecureRequests: true,
        },
    }));
}
app.use(cors({
    origin(origin, callback) {
        if (!origin) {
            return callback(null, true);
        }
        if (origin?.indexOf('http://localhost') === 0) {
            return callback(null, true);
        }
        if (![config.host, config.host + ':' + config.ports.webpack].includes(origin)) {
            return callback(new Error('The CORS policy for this site does not allow access from the specified Origin.'), false);
        }

        return callback(null, true);
    },
}));

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: true,
}));
app.use(bodyParser.json());

if (!config.production) {
    const mysql = require('./utils/mysql');
    const {getUserFromDB, parseUser} = require('./router/users');

    const dbID = 5;

    // app.use(async (req, res, next) => {
    //     const user = await parseUser(await getUserFromDB(dbID));
    //     const [scooters] = await mysql.query('SELECT dbID FROM Scooters WHERE userID = ? AND status != "disabled"', [dbID]);
    //     user.scooters = scooters.map(e => e.dbID);
    //     req.user = user;
    //     next(null);
    // });
}
app.use(expressSession);
app.use(passport.initialize());
app.use(passport.session());

const apiRouter = express.Router();
apiRouter.use(graphqlHTTP((req, res, params) => ({
    schema: apiSchema,
    graphiql: false,
    context: req,
})));
if (!config.production) {
    app.use('/graphiql', supergraphiqlExpress({
        endpointURL: '/graphql',
        subscriptionsEndpoint: `wss://${config.domain}/graphql`,
    }));
}

app.use('/auth', authRouter);
app.use('/graphql', apiRouter);
app.use('/static', express.static(config.staticPath));
if (config.production) {
    app.use('/', express.static(config.publicPath));
}
else {
    var proxy = require('express-http-proxy');
    app.use('/', proxy(`localhost:${config.ports.webpack}`));
}

let server = require('http').createServer();
server.on('request', app);

let wss = new WSServer({
    server,
});
SubscriptionServer.create(
    {
        schema: apiSchema,
        execute,
        subscribe,
        onConnect: (connectionParams, socket) => {
            return new Promise((resolve, reject) => {
                expressSession(socket.upgradeReq, {}, () => {
                    passport.initialize()(socket.upgradeReq, {}, () => {
                        passport.session()(socket.upgradeReq, {}, () => {
                            if (socket.upgradeReq.user) {
                                resolve(socket.upgradeReq);
                            }
                            else {
                                reject(new Error('not authenticated'));
                            }
                        });
                    });
                });
            });
        },
    },
    wss,
);

const serverHTTP = server.listen(config.ports.web, () => {
    const host = serverHTTP.address().address;
    const port = serverHTTP.address().port;

    log.info('HTTP Listening on', host, port);
});

const scooterServer = require('./scooter-server');