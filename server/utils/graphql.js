const {PubSub} = require('graphql-subscriptions');

function toGlobalID(options) {
    return new Buffer(JSON.stringify(options), 'utf8').toString('base64');
}
function fromGlobalID(globalIDBase64) {
    var globalID = new Buffer(globalIDBase64, 'base64').toString('utf8');
    var recovered = {};

    try {
        recovered = JSON.parse(globalID);
    }
    catch (e) {
        // console.log(e);
    }

    return recovered;
}

module.exports = {
    toGlobalID,
    fromGlobalID,
    pubsub: new PubSub(),
};