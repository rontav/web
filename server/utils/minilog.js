const Minilog = require('@rontav/minilog');
Minilog
    .pipe(Minilog.backends.console.formatColor)
    .pipe(Minilog.backends.console);

module.exports = (e) => {
    return Minilog(e + '\t');
};