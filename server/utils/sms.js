const config = require('../config');
const Nexmo = require('nexmo');

const nexmo = new Nexmo({
    ...config.nexmo,
});

module.exports = function sendSMS(to, text) {
    return new Promise((resolve, reject) => {
        nexmo.message.sendSms(config.appName, to, text, {}, (err, output) => {
            if (err) {
                return reject(err);
            }
            resolve(output);
        });
    });
};
