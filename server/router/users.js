const log = require('../utils/minilog')('Router > Users ');

const config = require('../config');
const mysql = require('../utils/mysql');

const {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
    GraphQLBoolean,
    GraphQLEnumType,
} = require('graphql');

async function getUserFromDB(userID) {
    var [[user]] = await mysql.query('SELECT * FROM Users WHERE dbID = ?', [userID]);
    return user || null;
}

function parseUser(user, {onlyPublicInfo} = {}) {
    if (!user?.dbID) {
        return user;
    }

    if (onlyPublicInfo) {
        user.googleID = undefined;
        user.facebookID = undefined;
        user.email = undefined;
    }
    return user;
}

const UserQueries = {
    type: new GraphQLObjectType({
        name: 'User',
        fields: {
            dbID: {
                type: GraphQLID,
            },
            name: {
                type: GraphQLString,
            },
            photo: {
                type: GraphQLString,
            },
            status: {
                type: new GraphQLEnumType({
                    name: 'UserStatus',
                    values: {
                        'active': {value: 'active'},
                        'disabled': {value: 'disabled'},
                    },
                }),
            },
        },
    }),
    resolve(parent, args, req) {
        return req.user;
    },
};

const UserWebPushMutation = {
    type: new GraphQLObjectType({
        name: 'UserWebPushMutation',
        fields: {
            done: {
                type: GraphQLBoolean,
            },
        },
    }),
    args: {
        input: {
            type: GraphQLString,
        },
    },
    resolve: async (parent, {input}, {user}) => {
        if (!user?.dbID) {
            throw new Error('invalid user');
        }
        if (!input) {
            throw new Error('invalid input');
        }
        JSON.parse(input);

        await mysql.query(' INSERT INTO PushSubscriptions (userID, subscription) VALUES (?, ?)', [user.dbID, input]);

        return {
            done: true,
        };
    },
};

const mutations = {
    userWebPush: UserWebPushMutation,
};

module.exports = {
    getUserFromDB,
    parseUser,
    UserQueries,
    mutations,
};