const log = require('../utils/minilog')('Router > Server ');

const config = require('../config');
const mysql = require('../utils/mysql');

const {
    GraphQLObjectType,
    GraphQLFloat,
} = require('graphql');

const ServerQueries = {
    type: new GraphQLObjectType({
        name: 'Server',
        fields: {
            serverTimestamp: {
                type: GraphQLFloat,
            },
        },
    }),
    async resolve() {
        const [[{serverTimestamp}]] = await mysql.query('SELECT UNIX_TIMESTAMP(CURRENT_TIMESTAMP) as serverTimestamp');

        return {
            serverTimestamp,
        };
    },
};

module.exports = {
    ServerQueries,
};