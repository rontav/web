const log = require('../utils/minilog')('Router > Scooters ');

const config = require('../config');
const mysql = require('../utils/mysql');
const {pubsub} = require('../utils/graphql');
const {withFilter} = require('graphql-subscriptions');
const {
    GraphQLInputObjectType,
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
    GraphQLBoolean,
    GraphQLInt,
    GraphQLFloat,
    GraphQLEnumType,
    GraphQLList,
    GraphQLNonNull,
} = require('graphql');
const GraphQLDate = require('graphql-date');
const fs = require('fs-extra');
const crypto = require('crypto');
const webpush = require('web-push');
const sendSMS = require('../utils/sms');
const smsPrivateKey = fs.readFileSync(config.scooters.caPath);

webpush.setVapidDetails(
    'mailto:mrvali97@gmail.com',
    config.vapid.publicKey,
    config.vapid.privateKey,
);

const ScooterStatusType = new GraphQLEnumType({
    name: 'ScooterStatus',
    values: {
        'not_registered': {value: 'not_registered'},
        'active': {value: 'active'},
        'stolen': {value: 'stolen'},
        'locked': {value: 'locked'},
        'disabled': {value: 'disabled'},
    },
});

const LocationFields = Object.fromEntries(['serverTimestamp', 'scooterTimestamp', 'latitude', 'longitude', 'altitude', 'speed', 'heading', 'hdop', 'pdop', 'vdop', 'satellitesInView', 'satellitesUsed', 'cn0Max'].map(e => [e, GraphQLFloat]));
const InfoFields = {
    'serverTimestamp': GraphQLFloat,
    'adseroVoltage': GraphQLFloat,
    'adseroTemperature': GraphQLFloat,
    'scooterSpeed': GraphQLFloat,
    'batteryLevel': GraphQLFloat,
    'powerGood': GraphQLInt,
    'locationStatus': GraphQLInt,
    'alarmStatus': GraphQLInt,
    'dashboardStatus': GraphQLInt,
    'scooterStatus': GraphQLInt,
    'adseroStatus': GraphQLInt,
};

const LocationType = new GraphQLNonNull(new GraphQLObjectType({
    name: 'Location',
    fields: Object.fromEntries(Object.keys(LocationFields).map(e => ([e, {type: LocationFields[e]}]))),
}));

const InfoType = new GraphQLNonNull(new GraphQLObjectType({
    name: 'Info',
    fields: Object.fromEntries(Object.keys(InfoFields).map(e => ([e, {type: InfoFields[e]}]))),
}));

const ScooterType = new GraphQLObjectType({
    name: 'Scooter',
    fields: {
        dbID: {
            type: GraphQLID,
        },
        userID: {
            type: GraphQLID,
        },
        name: {
            type: GraphQLString,
        },
        status: {
            type: ScooterStatusType,
        },
        created: {
            type: GraphQLDate,
        },
        location: {
            type: LocationType,
        },
        info: {
            type: InfoType,
        },
    },
});

const ScooterLocationUpdate = new GraphQLObjectType({
    name: 'ScooterLocationUpdate',
    fields: {
        dbID: {
            type: GraphQLID,
        },
        scooterID: {
            type: GraphQLID,
        },
        status: {
            type: GraphQLString,
        },
        location: {
            type: LocationType,
        },
        info: {
            type: InfoType,
        },
    },
});

const listScooters = {
    type: new GraphQLList(ScooterType),
    resolve: async (parent, args, {user}) => {
        if (!user?.dbID) {
            throw new Error('Not authenticated');
        }

        const [scooters] = await mysql.query(`
            SELECT
                S.*,
                ${Object.keys(LocationFields).map(e => `${e.toLowerCase().includes('timestamp') ? `UNIX_TIMESTAMP(L.${e})` : `L.${e}`} as location${e}`).join(',')},
                ${Object.keys(InfoFields).map(e => `${e.toLowerCase().includes('timestamp') ? `UNIX_TIMESTAMP(T.${e})` : `T.${e}`} as info${e}`).join(',')}
            FROM Scooters S
            LEFT JOIN (SELECT * FROM (SELECT * FROM ScooterInfoHeartbeats ORDER BY serverTimestamp DESC LIMIT 18446744073709551615) tt GROUP BY tt.scooterID) T ON T.scooterID = S.dbID
            LEFT JOIN (SELECT * FROM (SELECT * FROM ScooterLocationHeartbeats ORDER BY serverTimestamp DESC LIMIT 18446744073709551615) ttt GROUP BY ttt.scooterID) L ON L.scooterID = S.dbID
            WHERE
                S.userID = ?
            GROUP BY S.dbID
        `, [user.dbID]);

        scooters.map(scooter => {
            scooter.location = {};
            scooter.info = {};

            Object.keys(LocationFields).forEach(e => scooter.location[e] = scooter[`location${e}`]);
            Object.keys(InfoFields).forEach(e => scooter.info[e] = scooter[`info${e}`]);
        });

        return scooters;
    },
};

const scooterStatus = {
    type: ScooterStatusType,
    args: {
        scooterID: {
            type: GraphQLID,
        },
    },
    resolve: async (parent, {scooterID}, {user, isScooter}) => {
        if (!isScooter && !user?.scooters?.includes(scooterID)) {
            return;
        }

        const [[scooter]] = await mysql.query('SELECT status FROM Scooters WHERE dbID=?', [scooterID]);

        return scooter.status;
    },
};

const ScooterQueries = {
    type: new GraphQLObjectType({
        name: 'Scooters',
        fields: {
            list: listScooters,
            status: scooterStatus,
        },
    }),
    resolve() {
        return {};
    },
};

async function sendPushNotifications(scooterID) {
    const [[scooter]] = await mysql.query('SELECT userID FROM Scooters WHERE dbID = ?', [scooterID]);
    if (!scooter?.userID) {
        return {
            done: false,
        };
    }

    const [userPushSubs] = await mysql.query('SELECT dbID, subscription FROM PushSubscriptions WHERE userID = ? AND status = "active"', [scooter.userID]);

    return Promise.all(userPushSubs.map(async userSub => {
        try {
            const payload = 'Adse.ro tracker scooter theft alert!';
            try {
                const response = await webpush.sendNotification(JSON.parse(userSub.subscription), payload);
                await mysql.query('INSERT INTO PushHistory(subscriptionID, payload, response) VALUES(?, ?, ?)', [userSub.dbID, payload, JSON.stringify(response)]);
            }
            catch (err) {
                if (err.statusCode === 410) {
                    await mysql.query(`
                        UPDATE PushSubscriptions
                        SET status="disabled"
                        WHERE dbID=?
                    `, [userSub.dbID]);
                }
                else {
                    await mysql.query('INSERT INTO PushHistory(subscriptionID, payload, response) VALUES(?, ?, ?)', [userSub.dbID, payload, JSON.stringify(err)]);
                }
            }
        }
        catch (err) {
            console.error('Can\'t push to', userSub.dbID, err);
        }
    }));
}

const UpdateLocationAndStatusMutation = {
    type: new GraphQLObjectType({
        name: 'UpdateLocationAndStatusMutation',
        fields: {
            done: {
                type: GraphQLBoolean,
            },
        },
    }),
    args: {
        scooterID: {
            type: GraphQLID,
        },
        input: {
            type: new GraphQLInputObjectType({
                name: 'LocationAndStatusInput',
                fields: {
                    writeAdseroStatus: {
                        type: GraphQLInt,
                    },
                    ...Object.fromEntries(Object.keys(LocationFields).map(e => ([e, {type: LocationFields[e]}]))),
                    ...Object.fromEntries(Object.keys(InfoFields).map(e => ([e, {type: InfoFields[e]}]))),
                },
            }),
        },
    },
    resolve: async (parent, {scooterID, input}, {isScooter}) => {
        if (!isScooter && config.production) {
            return false;
        }
        const [[scooter]] = await mysql.query('SELECT status FROM Scooters WHERE dbID=?', [scooterID]);

        input['serverTimestamp'] = Math.round(Date.now() / 1000);

        input.adseroStatus = ['DISABLED', 'NOT_REGISTERED', 'STOLEN', 'LOCKED', 'ACTIVE'][input.adseroStatus]?.toLowerCase();
        if (input.writeAdseroStatus && scooter.status !== input.adseroStatus) {
            await Promise.all([
                mysql.query(`
                        UPDATE Scooters
                        SET status=?
                        WHERE dbID=?
                    `, [input.adseroStatus, scooterID]),
            ]);

            if (input.adseroStatus === 'stolen') {
                await sendPushNotifications(scooterID);
            }
        }

        const infoHeartbeat = {
            ...Object.fromEntries(Object.keys(InfoFields).map(key => [key, input[key]])),
            alarmStatus: input.alarmStatus ? 'on' : 'off',
            powerGood: input.powerGood ? 'yes' : 'no',
            dashboardStatus: input.dashboardStatus ? 'on' : 'off',
            scooterStatus: input.scooterStatus ? 'on' : 'standby',
            locationStatus: input.locationStatus ? 'fixed' : 'off',
        };

        let [{insertId}] = await mysql.query(`
            INSERT INTO ScooterInfoHeartbeats (scooterID, ${Object.keys(infoHeartbeat).join(',')})
            VALUES (?, ${Object.keys(infoHeartbeat).map((e) => e.toLowerCase().includes('timestamp') ? 'FROM_UNIXTIME(?)' : '?').join(', ')})
        `, [scooterID].concat(Object.keys(infoHeartbeat).map(key => infoHeartbeat[key])));

        if (input.scooterTimestamp) {
            await mysql.query(`
                INSERT INTO ScooterLocationHeartbeats (scooterID, ${Object.keys(LocationFields).join(',')})
                VALUES (?, ${Object.keys(LocationFields).map((e) => e.toLowerCase().includes('timestamp') ? 'FROM_UNIXTIME(?)' : '?').join(', ')})
            `, [scooterID].concat(Object.keys(LocationFields).map(key => input[key])));
        }

        if (insertId) {
            pubsub.publish('scooterLocationUpdate', {
                'scooterLocationUpdate': {
                    dbID: insertId,
                    scooterID,
                    status: input.writeAdseroStatus ? input.adseroStatus : scooter.status,
                    location: Object.fromEntries(Object.keys(LocationFields).map(key => [key, input[key]])),
                    info: infoHeartbeat,
                },
            });
        }

        return {
            done: true,
        };
    },
};

const RegisterScooterMutation = {
    type: new GraphQLObjectType({
        name: 'RegisterScooterMutation',
        fields: {
            done: {
                type: GraphQLBoolean,
            },
        },
    }),
    args: {
        scooterID: {
            type: GraphQLID,
        },
        input: {
            type: new GraphQLInputObjectType({
                name: 'RegisterScooterInput',
                fields: {
                    userId: {
                        type: GraphQLID,
                    },
                },
            }),
        },
    },
    resolve: async (parent, {scooterID, input}, {isScooter}) => {
        if (!isScooter && config.production) {
            return false;
        }
        if (!input?.userId || !scooterID) {
            return;
        }

        const [[scooter]] = await mysql.query('SELECT status FROM Scooters WHERE dbID=?', [scooterID]);
        if (scooter?.status !== 'not_registered') {
            return {
                done: false,
            };
        }

        const [result] = await mysql.query(`
            UPDATE Scooters
            SET userID=?, status="active"
            WHERE dbID=?
        `, [input.userId, scooterID]);

        return {
            done: result?.affectedRows >= 1,
        };
    },
};

const LockScooterMutation = {
    type: new GraphQLObjectType({
        name: 'LockScooterMutation',
        fields: {
            smsSent: {
                type: GraphQLBoolean,
            },
        },
    }),
    args: {
        scooterID: {
            type: GraphQLID,
        },
    },
    resolve: async (parent, {scooterID}, {user}) => {
        if (!user?.scooters?.includes(scooterID)) {
            throw new Error('not your scooter');
        }

        const [[scooter]] = await mysql.query('SELECT dbID, phoneNumber FROM Scooters WHERE dbID = ?', [scooterID]);
        if (!scooter?.dbID) {
            throw new Error('scooter not in db');
        }

        const data = 'STOLEN' + Date.now();
        let encryptedData = crypto.privateEncrypt(smsPrivateKey, Buffer.from(data)).toString('base64');
        const messageSize = 153 - 3;
        let sms = '';
        for (let i = 0; i < encryptedData.length / messageSize; i++) {
            sms += `!${i}!` + encryptedData.substr(i * messageSize, messageSize);
        }
        sms += '!';

        console.log(sms, sms.length);
        try {
            if (config.production) {
                const response = await sendSMS(scooter.phoneNumber, sms);

                if (response['message-count'] > 0) {
                    await mysql.query(`
                        UPDATE Scooters
                        SET status="stolen"
                        WHERE dbID=?
                    `, [scooter.dbID]);

                    return {
                        smsSent: true,
                    };
                }
            }
            else {
                log.info('Send SMS to scooterID', scooter.dbID);
                await mysql.query(`
                    UPDATE Scooters
                    SET status="stolen"
                    WHERE dbID=?
                `, [scooter.dbID]);
                return {
                    smsSent: true,
                };
            }
        }
        catch (err) {
            console.error(err);
        }
        return {
            smsSent: false,
        };
    },
};

const UnlockScooterMutation = {
    type: new GraphQLObjectType({
        name: 'UnlockScooterMutation',
        fields: {
            done: {
                type: GraphQLBoolean,
            },
        },
    }),
    args: {
        scooterID: {
            type: GraphQLID,
        },
    },
    resolve: async (parent, {scooterID}, {user}) => {
        if (!user?.scooters?.includes(scooterID)) {
            throw new Error('not your scooter');
        }

        const [[scooter]] = await mysql.query('SELECT dbID, status FROM Scooters WHERE dbID = ?', [scooterID]);
        if (!scooter?.dbID) {
            throw new Error('scooter not in db');
        }
        if (scooter.status === 'stolen' || scooter.status === 'locked') {
            await mysql.query(`
                UPDATE Scooters
                SET status="active"
                WHERE dbID=?
            `, [scooter.dbID]);
        }

        return {
            done: true,
        };
    },
};

const ScooterStolenMutation = {
    type: new GraphQLObjectType({
        name: 'ScooterStolenMutation',
        fields: {
            done: {
                type: GraphQLString,
            },
        },
    }),
    args: {
        scooterID: {
            type: GraphQLID,
        },
    },
    resolve: async (parent, {scooterID}, {isScooter}) => {
        if (!isScooter && config.production) {
            return {
                done: false,
            };
        }

        await sendPushNotifications();
        await mysql.query(`
            UPDATE Scooters
            SET status="stolen"
            WHERE dbID=?
        `, [scooterID]);

        return {
            done: true,
        };
    },
};

const mutations = {
    updateLocationAndStatus: UpdateLocationAndStatusMutation,
    registerScooter: RegisterScooterMutation,
    lockScooter: LockScooterMutation,
    unlockScooter: UnlockScooterMutation,
    scooterStolen: ScooterStolenMutation,
};
const subscriptions = {
    scooterLocationUpdate: {
        type: ScooterLocationUpdate,
        subscribe: withFilter(() => pubsub.asyncIterator('scooterLocationUpdate'), (payload, variables, {user}) => {
            return user?.scooters?.includes(payload?.scooterLocationUpdate?.scooterID);
        }),
    },
};

module.exports = {
    ScooterQueries,
    mutations,
    subscriptions,
};