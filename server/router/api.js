const {
    GraphQLSchema,
    GraphQLObjectType,
} = require('graphql');

const {
    UserQueries,
    mutations: userMutations,
} = require('./users');
const {
    ScooterQueries,
    mutations: scooterMutations,
    subscriptions: scooterSubscriptions,
} = require('./scooter');
const {ServerQueries} = require('./server');

var apiSchema = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'RootQueryType',
        fields: {
            viewer: {
                type: new GraphQLObjectType({
                    name: 'Viewer',
                    fields: {
                        user: UserQueries,
                        scooters: ScooterQueries,
                        server: ServerQueries,
                    },
                }),
                resolve() {
                    return {};
                },
            },
        },
    }),
    mutation: new GraphQLObjectType({
        name: 'RootMutationType',
        fields: {
            ...userMutations,
            ...scooterMutations,
        },
    }),
    subscription: new GraphQLObjectType({
        name: 'RootSubscriptionType',
        fields: {
            ...scooterSubscriptions,
        },
    }),
});

module.exports = apiSchema;