const {printSchema} = require('graphql/utilities');
const apiSchema = require('./router/api');
const path = require('path');
const fs = require('fs');

const graphQLFile = path.join(__dirname, '../schema.graphql');
fs.writeFileSync(graphQLFile, printSchema(apiSchema));
process.exit(0);