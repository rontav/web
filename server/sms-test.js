const db = require('./services/mysql');
const sendSMS = require('./services/sms');

const to = '+40749899829';
const text = 'Custom test message';

(async () => {
    // console.log(await sendSMS(to, text));
    console.log(await db.query('SELECT * FROM index'));
})();