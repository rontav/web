const log = require('./utils/minilog')('Passport ');

const config = require('./config');

const fs = require('fs-extra');
const path = require('path');
const url = require('url');
const express = require('express');
const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;

const bent = require('bent');
const getBuffer = bent('buffer');

const mysql = require('./utils/mysql');

const {getUserFromDB, parseUser} = require('./router/users');

fs.ensureDir(config.staticPath);

async function cacheSocialPhoto(profile, user) {
    try {
        const photo = profile?.photos?.[0]?.value;

        if (!user.dbID || !photo) {
            console.log('no user/no photo');
            return;
        }

        let filename = photo;
        if (filename.includes('lookaside')) {
            filename = filename.substr(filename.indexOf('asid=') + 'asid='.length);
            filename = filename.includes('&') ? filename.substr(0, filename.indexOf('&')) : filename;
        }
        else {
            filename = path.basename(url.parse(photo).pathname);
        }

        if (filename === user.photo && !user.photo.includes('http')) {
            // same photo, already cached
            console.log('already cached');
            return;
        }

        await fs.writeFile(path.resolve(config.staticPath, filename), await getBuffer(photo));

        await mysql.query('UPDATE Users SET photo = ? WHERE dbID = ?', [filename, user.dbID]);
    }
    catch (err) {
        log.debug('Failed to cache profile image of', profile, user, err);
    }
    console.log('done');
}

async function handleSocialLogin(profile, socialNetwork) {
    if (!profile?.id) {
        throw new Error('No profile');
    }

    var [[user]] = await mysql.query(`SELECT * FROM Users WHERE ${socialNetwork}ID = ?`, [profile.id]);

    if (!user) {
        await mysql.query(`
            INSERT INTO Users (${socialNetwork}ID, email, name, photo)
            VALUES (?, ?, ?, ?)
        `, [
            profile.id,
            profile?.emails?.[0]?.value,
            profile.displayName,
            profile?.photos?.[0]?.value,
        ]);

        [[user]] = await mysql.query(`SELECT * FROM Users WHERE ${socialNetwork}ID = ?`, [profile.id]);
    }
    if (profile?.photos?.[0]?.value && (!user?.photo || user?.photo?.includes('http'))) {
        cacheSocialPhoto(profile, user);
    }

    return user;
}
passport.use(new FacebookStrategy({
    clientID: config.facebookAuth.clientID,
    clientSecret: config.facebookAuth.clientSecret,
    callbackURL: config.facebookAuth.callbackURL,
    profileFields: ['id', 'displayName', 'photos', 'email'],
}, async (accessToken, refreshToken, profile, cb) => {
    try {
        const user = await handleSocialLogin(profile, 'facebook');
        cb(null, user);
    }
    catch (err) {
        cb(err, {});
    }
}));
passport.use(new GoogleStrategy({
    clientID: config.googleAuth.clientID,
    clientSecret: config.googleAuth.clientSecret,
    callbackURL: config.googleAuth.callbackURL,
}, async (accessToken, refreshToken, profile, cb) => {
    try {
        const user = await handleSocialLogin(profile, 'google');
        cb(null, user);
    }
    catch (err) {
        cb(err, {});
    }
}));

passport.serializeUser((user, cb) => {
    cb(null, user.dbID);
});

passport.deserializeUser(async (dbID, cb) => {
    if (!config.production && dbID) {
        dbID = 5;
    }

    try {
        const user = await parseUser(await getUserFromDB(dbID));
        const [scooters] = await mysql.query('SELECT dbID FROM Scooters WHERE userID = ? AND status != "disabled"', [dbID]);
        user.scooters = scooters.map(e => e.dbID.toString());
        cb(null, user);
    }
    catch (e) {
        return cb(e);
    }
});

var authRouter = express.Router();

authRouter.get('/facebook', (req, res, next) => {
    req.session.returnTo = req.query.returnTo;
    next();
});
authRouter.get('/google', (req, res, next) => {
    req.session.returnTo = req.query.returnTo;
    next();
});
authRouter.get('/facebook', passport.authenticate('facebook', {
    scope: ['public_profile', 'email'],
}));
authRouter.get('/facebook/callback',
    passport.authenticate('facebook'), (req, res) => {
        if (!req.user.status) {
            req.logout();
            return res.redirect('/login?disabled=1&timestamp' + Date.now());
        }
        res.redirect(req.session.returnTo || '/');
    });

authRouter.get('/google', passport.authenticate('google', {
    scope: ['profile', 'email'],
}));
authRouter.get('/google/callback',
    passport.authenticate('google'), (req, res) => {
        if (!req.user.status) {
            req.logout();
            return res.redirect('/login?disabled=1&timestamp' + Date.now());
        }
        res.redirect(req.session.returnTo || '/');
    });

authRouter.get('/logout', (req, res) => {
    if (req.user) {
        req.logout();
    }
    res.redirect('/');
});


module.exports = {
    passport,
    authRouter,
};