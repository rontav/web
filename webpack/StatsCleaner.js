// get rid of mini-css-extract-plugin logs
// https://github.com/webpack-contrib/mini-css-extract-plugin/issues/168

module.exports = class CleanUpStatsPlugin {
    shouldPickStatChild(child) {
        return child.name.indexOf('extract-css-chunks-webpack-plugin') !== 0;
    }

    apply(compiler) {
        compiler.hooks.done.tap('CleanUpStatsPlugin', (stats) => {
            const children = stats.compilation.children;
            if (Array.isArray(children)) {
                // eslint-disable-next-line no-param-reassign
                stats.compilation.children = children
                    .filter(child => this.shouldPickStatChild(child));
            }
        });
    }
};