const path = require('path');
const config = require('../server/config');
const webpackConfig = require('./webpack.common');
const CleanUpStatsPlugin = require('./StatsCleaner.js');
const RelayCompilerWebpackPlugin = require('relay-compiler-webpack-plugin');
// const CpuProfilerWebpackPlugin = require('cpuprofile-webpack-plugin');

const HtmlWebpackPlugin = require('html-webpack-plugin');

webpackConfig.module.rules[1].use = 'url-loader';
webpackConfig.module.rules[2].use = 'url-loader';

webpackConfig.module.rules = webpackConfig.module.rules.concat([
    {
        test: /\.module\.(sa|sc|c)ss$/,
        use: [
            'style-loader',
            {
                loader: 'css-loader',
                options: {
                    modules: {
                        localIdentName: '[local]__hash',
                    },
                },
            },
            'postcss-loader',
            'sass-loader',
        ],
    },
]);

Object.assign(webpackConfig, {
    mode: 'development',
    devServer: {
        host: '0.0.0.0',
        port: config.ports.webpack,
        disableHostCheck: true,
        contentBase: path.resolve(__dirname, '../frontend'),
        historyApiFallback: true,
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        clientLogLevel: 'warning',
    },
    devtool: 'cheap-module-source-map',
});
webpackConfig.plugins = webpackConfig.plugins.concat([
    new HtmlWebpackPlugin({
        template: 'frontend/index.html',
        chunks: ['bundle'],
    }),
    new RelayCompilerWebpackPlugin({
        schema: path.resolve(__dirname, '../schema.graphql'),
        src: path.resolve(__dirname, '../frontend'),
    }),
    new CleanUpStatsPlugin(),
]);

module.exports = webpackConfig;