const webpack = require('webpack');
const webpackConfig = require('./webpack.common');

const ExtractCssChunks = require('extract-css-chunks-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;

webpackConfig.mode = 'production';

webpackConfig.module.rules = webpackConfig.module.rules.concat([
    {
        test: /\.module\.(sa|sc|c)ss$/,
        use: [
            {
                loader: ExtractCssChunks.loader,
                options: {
                    hot: false,
                },
            },
            {
                loader: 'css-loader',
                options: {
                    modules: {
                        localIdentName: '[local]__[hash:base64:5]',
                    },
                },
            },
            'postcss-loader',
            'sass-loader',
        ],
    },
]);
webpackConfig.plugins = webpackConfig.plugins.concat([
    new HtmlWebpackPlugin({
        template: 'frontend/index.prod.html',
        filename: 'index.html',
        chunks: ['bundle'],
    }),
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify('production'),
        },
    }),
    new ImageminPlugin({
        test: /\.(png|jpg|jpeg|gif)/,
    }),
    new OptimizeCssAssetsPlugin({
        assetNameRegExp: /\.css$/g,
        cssProcessor: require('cssnano'),
        cssProcessorPluginOptions: {
            preset: ['default', {
                calc: false,
                discardComments: {
                    removeAll: true,
                },
            }],
        },
    }),
]);

webpackConfig.optimization = {
    splitChunks: {
        cacheGroups: {
            vendors: {
                test: /[\\/]node_modules[\\/]/,
                name: 'vendors',
                chunks: (chunk) => chunk.name === 'bundle',
            },
        },
    },
};

module.exports = webpackConfig;