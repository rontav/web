const webpack = require('webpack');
const path = require('path');
const ExtractCssChunks = require('extract-css-chunks-webpack-plugin');

const devMode = process.env.NODE_ENV !== 'production';

let exp = {
    context: path.join(__dirname, '../'),
    entry: {
        bundle: './frontend/index.jsx',
    },
    output: {
        path: path.join(__dirname, '../dist/'),
        filename: devMode ? '[name].js' : '[name].[contenthash].js',
        publicPath: '/',
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            plugins: [
                                'react-hot-loader/babel',
                                '@babel/plugin-proposal-object-rest-spread',
                                ['@babel/plugin-proposal-decorators', {
                                    'legacy': true,
                                }],
                                ['@babel/plugin-proposal-class-properties', {
                                    'loose': true,
                                }],
                                '@babel/proposal-optional-chaining',
                                'relay',
                            ],
                            presets: [
                                ['@babel/preset-env', {
                                    corejs: '3.1',
                                    useBuiltIns: 'usage',
                                    targets: ['> 1%', 'last 4 versions'],
                                    modules: false,
                                }],
                                '@babel/react',
                            ],
                        },
                    },
                ],
            },
            {
                test: /\.(png|jpg|gif?)/,
                use: 'file-loader?name=[name].[contenthash].[ext]',
            },
            {
                test: /\.(eot|svg|ttf|otf|woff(2)?)(\?v=\d+\.\d+\.\d+)?/,
                use: 'file-loader?name=[name].[contenthash].[ext]',
            },
            {
                test: /^(?!.*?\.module).*\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: ExtractCssChunks.loader,
                        options: {
                            hot: process.env.REACT_HOT,
                        },
                    },
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ],
            },
        ],
    },
    plugins: [
        new webpack.EnvironmentPlugin({
            REACT_HOT: false,
        }),
        new ExtractCssChunks({
            filename: devMode ? '[name].css' : '[name].[contenthash].css',
        }),
    ],
    resolve: {
        alias: {
            'react-dom': '@hot-loader/react-dom',
            'scss-variables': path.join(__dirname, '../frontend/scss/_variables.scss'),
        },
        extensions: ['.js', '.jsx'],
    },
};

module.exports = exp;